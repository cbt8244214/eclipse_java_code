
public class Diamond { 
	public static void main(String args[]){
		int n=5;
		for(int a=1;a<=(n+1)/2;a++){
			for(int b=1;b<=(n+1)/2-a;b++){
				System.out.print(" ");
			}
			for(int i=1;i<=a;i++){
				System.out.print("* ");
			}
			System.out.println();
		}
		for(int a=(n+1)/2;a>1;a--){
			for(int b=1;b<=(n+1)/2-a;b++){
				System.out.print(" ");
			}
			for(int i=1;i<a;i++){
				System.out.print(" *");
			}
			System.out.println();
		}
	}

}

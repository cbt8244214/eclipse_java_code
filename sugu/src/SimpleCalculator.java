import java.util.Scanner;
public class SimpleCalculator {
	public static void main(String args[]){
		Scanner ob=new Scanner(System.in);
		System.out.print("Enter two numbers");
			double first=ob.nextDouble();
			double second=ob.nextDouble();
		System.out.print("Enter an operator(+,-,*,/):");
			char operator=ob.next().charAt(0);
				 double result;
				switch(operator){
				case '+':
					result=first+second;
					break;
				case '-':   
					result=first-second;
					break;
				case '*':
					result=first*second;
					break;
				case '/':
					result=first/second;
					break;
				default:
					System.out.println("Error operator is not correct");
						return;
				}
				System.out.println(first+" "+operator+" "+second+" ="+result);
	}
}

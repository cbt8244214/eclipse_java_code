
public class GFG {
	static final int MAX=256;
	static String lastNonReapeating(String str,int n){
		int freq[]=new int[MAX];
		for(int i=0;i<n;i++)
			freq[str.charAt(i)]++;
		for(int i=n-1;i>=0;i--){
			char ch=str.charAt(i);
			if(freq[ch]==1)
				return (" "+ch);
		}
		return "-1";
	}
	public static void main(String args[]){
		String str="GeeksForGeeks";
		int n=str.length();
		System.out.println(lastNonReapeating(str,n));
	}
}

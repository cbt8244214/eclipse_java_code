
public class Recursion {
	public static void main(String args[]){
		int num=1234;
		System.out.print("reverse of the number is=");
		reversemeth(num);
	}
	public static void reversemeth(int n){
		if(n<10){
			System.out.print(n);
			return;
		}
		else{
			System.out.print(n%10);
			reversemeth(n/10);
		}
	}
	
}

class VarArgsOverload {
static void meth(int...v){
System.out.print("meth(int ...)"+"Number of args"+v.length+"Contents");
for(int x:v){
System.out.print(x+" ");
}
System.out.println();
}
static void meth(String msg,int...v){
System.out.print("meth(String,int ...)"+msg+v.length+"Contents");
for(int x:v){
System.out.print(x+" ");
}
System.out.println();
}
public static void main(String args[]){
meth(1,2,3);
meth("Testing",10,20);
meth("Testing");
}
}

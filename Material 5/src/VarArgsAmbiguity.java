public class VarArgsAmbiguity {
static void meth(int...v){
System.out.print("meth(int ...)"+"Number of args"+v.length+"Contents");
for(int x:v){
System.out.print(x+" ");
}
System.out.println();
}
static void meth(boolean...v){
System.out.print("meth(boolean ...)"+"Number of args"+v.length+"Contents");
for(boolean x:v){
System.out.print(x+" ");
}
System.out.println();
}
public static void main(String args[]){
meth(1,2,3);
meth(true,false,false);
//meth();
}
}

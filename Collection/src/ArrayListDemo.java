import java.util.ArrayList;
import java.util.List;
public class ArrayListDemo {
	public static void main(String args[]){
		ArrayList al=new ArrayList();
		al.add(20);
		al.add(35);
		al.add(33);
		al.add(5);
		al.add('A');
		al.add(false);
		al.add("sugu");
		al.add(3, 88);
		al.add(55);
		al.remove(2);
		System.out.println(al);
		/*System.out.println(al.add(99));
		System.out.println(al);
		System.out.println(al.size());*/
		//System.out.println(al.contains("sugu"));
	//	//System.out.println(al.contains("AK"));
		System.out.println(al.get(0));
		al.set(6, 832);
		System.out.println(al);
		System.out.println("Index "+al.indexOf(5));
		System.out.println(al.remove(5));
		System.out.println("al list "+al);
		ArrayList al2=new ArrayList();
		al2.addAll(al);
		System.out.println("al2 list " +al2);
		ArrayList al3=new ArrayList();
		al3.add("bhuvana");
		al3.add("Kavi");
		al3.add("viji"); 
		al3.add("menaka");
		System.out.println("al3 list "+al3);
		al3.addAll(3, al);
		System.out.println("al3 after addition "+ al3);
		List al4=al3.subList(0, 3);
		System.out.println("al4 lost is "+al4);
		al3.removeAll(al);
		System.out.println("al3 after remove all "+al3);
	}
}

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class CountWords1 {
	public static void main(String args[]){
		String s1="innocentboy";
		char[] c1=s1.toCharArray();
		HashMap<Character,Integer>hm=new HashMap<Character,Integer>();
		for(char c:c1){
			if(hm.containsKey(c)){
				hm.put(c,hm.get(c)+1);
			}else{
				hm.put(c, 1);
			}
		}
		System.out.println(hm);
		Set<Map.Entry<Character,Integer>>s=hm.entrySet();
		for(Map.Entry<Character, Integer>se:s){
			if(se.getValue()==1){
				System.out.println(se.getKey()+" "+se.getValue());
			}
		}
	}
}

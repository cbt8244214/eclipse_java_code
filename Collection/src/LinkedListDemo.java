import java.util.LinkedList;
public class LinkedListDemo {
	public static void main (String args[]){
		LinkedList ll=new LinkedList();
		ll.add(5);
		ll.add(22);
		ll.add("Ajith");
		ll.add(false);
		System.out.println(ll);
		ll.addFirst(100);
		System.out.println(ll);
		System.out.println(ll.poll());
		System.out.println(ll);
		ll.offer(5000);
		System.out.println(ll);
		LinkedList ll2=(LinkedList)ll.clone();
		System.out.println(ll2);
	}
}

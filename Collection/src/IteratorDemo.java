import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
public class IteratorDemo {
	public static void main(String args[]){
		ArrayList <String>al=new ArrayList<String>();
		al.add("Sugu");
		al.add("Bhuvana");
		al.add("Kavi");
		al.add("Idly");
		Iterator <String>itr=al.iterator();
		while(itr.hasNext()){
			String element =itr.next();
			System.out.print(element+" ");
		}
		System.out.println();
		ListIterator <String>litr=al.listIterator();
		while(litr.hasNext()){
			String element=litr.next();
			System.out.print(element+" +");
		}
		System.out.println();
		while(litr.hasPrevious()){
			String element=litr.previous();
			System.out.print(element+" ");
		}
	}
}

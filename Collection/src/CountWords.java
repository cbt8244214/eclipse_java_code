import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
public class CountWords {
	public static void main(String args[]){
		String s1="innocentboy";
		char[] ch=s1.toCharArray();     //a m m a a p p a
		/*for(char c:ch){
			System.out.println(c);
		}*/
		LinkedHashMap <Character,Integer> CountWord=new LinkedHashMap<Character,Integer>();
		for(char c:ch){		//c= a m m a a p p a
			if(CountWord.containsKey(c)){
			CountWord.put(c,CountWord.get(c)+1);	//using get()  we can retray 
			}else									//the corresponding value of key.
			CountWord.put(c,1);
		}
		System.out.println(CountWord);
			Set<Map.Entry<Character,Integer>> s=CountWord.entrySet();
			for(Map.Entry<Character,Integer>entry:s){
				if(entry.getValue()==1){
				System.out.println(entry.getKey()+" "+entry.getValue());
				}
			}
	}
}

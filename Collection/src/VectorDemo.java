import java.util.Enumeration;
import java.util.Vector;


public class VectorDemo {
	public static void main(String args[]){
		Vector <Integer>v=new Vector<Integer>(3,2);
		System.out.println("initial size "+v.size()+" and capacity "+v.capacity());
		v.addElement(102);
		v.addElement(108);
		v.addElement(142);
		v.addElement(102);
		System.out.println(v);
		System.out.println(v.capacity());
		v.add(185);
		System.out.println(v.capacity());
		v.addElement(136);
		System.out.println(v.capacity());
		System.out.println(v);
		System.out.println(v.contains(22));
		System.out.println(v.contains(108));
		System.out.println(v.firstElement());
		System.out.println(v.lastIndexOf(102));
		System.out.println(v.lastElement());
		Enumeration<Integer> Enum=v.elements();
		while(Enum.hasMoreElements()){
			System.out.print(Enum.nextElement()+" ");
		}
	}
}

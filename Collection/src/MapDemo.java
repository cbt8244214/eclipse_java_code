import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
public class MapDemo {
	public static void main(String args[]){
		Map<String,Integer>m=new HashMap<String,Integer>();
		//Map<String,Integer>m=new LinkedHashMap<String,Integer>();
		//Map<String,Integer>m=new TreeMap<String,Integer>();
		m.put("sugu", 100);
		m.put("Bhuvana", 101);
		m.put("kavi", 102);
		m.put("viji", 103);
		//System.out.println(m);
		Set<Map.Entry<String,Integer>>s=m.entrySet();
		for(Map.Entry<String,Integer>s1:s){
			System.out.print(s1.getKey()+" "+s1.getValue()+" ,");
		}
		System.out.println();
		int a=m.get("sugu");
		System.out.println(a);
		m.put("sugu",a+22);
		System.out.println(m.get("sugu"));
		System.out.println(m);
		
	}
}

//user vs computer.
import java.util.*;

public class TicTagToe {
	static ArrayList<Integer> playerpositions=new ArrayList<Integer>();
	static ArrayList<Integer> cpupositions=new ArrayList<Integer>();
	static char [][] gameBoard=new char[3][3];
	public static void main(String args[]){
		initializeBoard();
		win();
			
	}
	
	public static void win(){
		while(true){
			Scanner in=new Scanner(System.in);
			System.out.print("\nEnter your placement (1-9):");
			int playerpos=in.nextInt();
			
			while(playerpositions.contains(playerpos)||cpupositions.contains(playerpos)){
				System.out.println("Position taken! Enter a correct position");
				playerpos=in.nextInt();
			}
			
			placepiece(gameBoard,playerpos,"player");
			
			
			
			String result=checkWinner();
			printGameBoard();
			if(result.length()>0){
				System.out.println(result);
				playerpositions.removeAll(playerpositions);
				cpupositions.removeAll(cpupositions);
				playagain();
			}
			
			Random rand=new Random();
			int cpupos=rand.nextInt(9)+1;
			
			while(playerpositions.contains(cpupos)||cpupositions.contains(cpupos)){
				cpupos=rand.nextInt(9)+1;
			}
			System.out.println("\nCPU choose "+cpupos+"\n");
			
			placepiece(gameBoard,cpupos,"cpu");
			
			
			
			result=checkWinner();
			printGameBoard();
			if(result.length()>0){
				System.out.println(result);
				playerpositions.removeAll(playerpositions);
				cpupositions.removeAll(cpupositions);
				playagain();
			}
		}
	}
	public static void initializeBoard(){
		char a='1';
		 for (int i = 0; i < gameBoard.length; i++) {
		        for (int j = 0; j < gameBoard.length; j++) {
		            gameBoard[i][j] = a;
		            a++;
		        }
		    }
		printGameBoard();
	}
	public static void printGameBoard(){

		/*for(char[] row: gameBoard){
			for(char c:row){
				System.out.print(c);
			}
			System.out.println();
		}*/
		for (int i = 0; i < gameBoard.length; i++) {  
	        for (int j = 0; j < gameBoard.length; j++) {
	            if (j == 0) {
	                System.out.print("| ");
	            }
	            System.out.print(gameBoard[i][j] + " | ");
	        }
	        System.out.println();
	    }
	}
	public static void placepiece(char[][] gameBoard,int pos, String user){
		char symbol=' ';
		if(user.equals("player")){
			symbol='X';	
			playerpositions.add(pos);
		}else if(user.equals("cpu")){
			symbol='O';
			cpupositions.add(pos); 
		}
		switch(pos){
			case 1:
				gameBoard[0][0]=symbol;
				break;
			case 2:
				gameBoard[0][1]=symbol;
				break;
			case 3:
				gameBoard[0][2]=symbol;
				break;
			case 4:
				gameBoard[1][0]=symbol;
				break;
			case 5:
				gameBoard[1][1]=symbol;
				break;
			case 6:
				gameBoard[1][2]=symbol;
				break;
			case 7:
				gameBoard[2][0]=symbol;
				break;
			case 8:
				gameBoard[2][1]=symbol;
				break;
			case 9:
				gameBoard[2][2]=symbol;
				break;
				default:
					break;
		}
	}
	public static String checkWinner(){
		List topRow=Arrays.asList(1,2,3);
		List midRow=Arrays.asList(4,5,6);
		List botRow=Arrays.asList(7,8,9);
		List leftCol=Arrays.asList(1,4,7);
		List midCol=Arrays.asList(2,5,8);
		List rightCol=Arrays.asList(3,6,9);
		List cross1=Arrays.asList(1,5,9);
		List cross2=Arrays.asList(7,5,3);
		
		List<List>winning=new ArrayList<List>();
		winning.add(topRow);
		winning.add(midRow);
		winning.add(botRow);
		winning.add(leftCol);
		winning.add(midCol);
		winning.add(rightCol);
		winning.add(cross1);
		winning.add(cross2);
		
		for(List l:winning){
			if(playerpositions.containsAll(l)){
				return "Congratulation you won!:";
			}else if(cpupositions.containsAll(l)){
				return "CPU win! :";
			}else if(playerpositions.size()+cpupositions.size()==9);
				return "Match draw";
		}
		
		return "";
	} 
	 public static void playagain() {
	    	String ch;
	            System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
	            Scanner in =new Scanner(System.in);
	            ch=in.nextLine();
	            if((ch.equalsIgnoreCase("yes"))){
	            	initializeBoard();
	        		win();
	            }
	            else if (!(ch.equalsIgnoreCase("yes"))) {
	                System.out.println("Bye!");
	                System.exit(0);  // terminate the program
	             }
	    }
}


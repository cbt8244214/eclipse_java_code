import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
public class TicTag3 {
	static String []board;
	static String turn;
	static String checkwinner(){
		String line=null;
		for(int a=0;a<8;a++){
			switch (a){
				case 0:
					line=board[0]+board[1]+board[2];
					break;
				case 1:
					line=board[3]+board[4]+board[5];
					break;
				case 2:
					line=board[6]+board[7]+board[8];
					break;
				case 3:
					line=board[0]+board[3]+board[6];
					break;
				case 4:
					line=board[1]+board[4]+board[7];
					break;
				case 5:
					line=board[2]+board[5]+board[8];
					break;
				case 6:
					line=board[0]+board[4]+board[8];
					break;
				case 7:
					line=board[2]+board[4]+board[6];
					break;
				}
				if(line.equals("XXX")){
					return "X";
				}else if(line.equals("OOO")){
					return "O";
			}	
		}
		for(int a=0;a<9;a++){
			if(Arrays.asList(board).contains(String.valueOf(a+1))){
				break;
			}else if(a==8){
				return "draw";
			}	
		}
		System.out.println(turn+"'s turn to slot the number"+turn+" in :");
		return null;
	}
		
	static void printBoard(){
			System.out.println("|---|---|---|"); 
	        System.out.println("| " + board[0] + " | "+ board[1] + " | " + board[2]+ " |"); 
	        System.out.println("|-----------|"); 
	        System.out.println("| " + board[3] + " | "+ board[4] + " | " + board[5]+ " |"); 
	        System.out.println("|-----------|"); 
	        System.out.println("| " + board[6] + " | "+ board[7] + " | " + board[8]+ " |"); 
	        System.out.println("|---|---|---|");
	}
	public static void main(String args[]){
		board=new String[9];
		turn="X";
		String winner=null;
		Scanner in=new Scanner(System.in);
		for(int a=0;a<9;a++){
			board[a]=String.valueOf(a+1);
		}
		System.out.println("welcome to tic tag toe");
		printBoard();
		System.out.println("X will play  first ; enter the number to slot the number X in");
		while(winner==null){
			int num=in.nextInt();
			try{
				if(!(num > 0 && num <=9)){
					System.out.println("invalid input ; re-enter the slot number");
					continue;
				}
			}catch (InputMismatchException e){
				System.out.println("invalid input ; re-enter the slot number");
				continue;
			}
			if(board[num-1].equals(String.valueOf(num))){
				board[num-1]=turn;
				if(turn.equals("X")){
					turn="O";
				}else{
					turn="X";
				}
			}else{
				System.out.println("This slot is already taken : re-enter the slot number");
			}
			printBoard();
			winner=checkwinner();
		}
		if(winner.equalsIgnoreCase("draw")){
			System.out.println("Match is draw ; Thank you for playing ");
		}else
			System.out.println("Congratulation "+winner+" is a winner ; Thank you for playing ");
	}
}

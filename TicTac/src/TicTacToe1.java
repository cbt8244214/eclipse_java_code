
import java.util.Random;
import java.util.Scanner;

public class TicTacToe1 {

public static void main(String[] args) {
    welcome();
    initializeBoard();
    printBoard();
    win();
   
    System.out.println();
    winner();
	
}

static String[][] board = new String[3][3];

static int row, column;

static Scanner scan = new Scanner(System.in);

public static String currentTurn ;

//static String computerTurn = "O";

public static String turn() {
    if (currentTurn == "X") {
        currentTurn = "O";
    } else {
        currentTurn = "X";
    }
    return currentTurn;
}
public static String getplayer(){
	return currentTurn;
}
private static void win(){
	 while((checkwin()==null) && (checkDraw()==null)) {
		    if ((checkwin()==null) && (checkDraw()==null)) {
		        playerMove();
		        printBoard();
		        System.out.println();
		    }
		     if ((checkwin()==null) && (checkDraw()==null)) {
		        computerMove();
		        printBoard();
		        System.out.println();
		    }
		    }
}
private static void winner(){
	if(((checkwin().equals("X"))||(checkwin().equals("O")))&&((checkDraw()==null))){
		System.out.println("Congratulation "+turn()+" is a winner ; Thank you for playing ");
		playagain();
	}else {
		System.out.println("Match is draw ; Thank you for playing ");
		playagain();
	}
}
private static void welcome() {
    System.out.println("Tic Tac Toe");
    System.out.println("Please enter your coordinates for your location row (0-2) column (0-2):");
}

public static void initializeBoard() { // initialize tic tac toe
	currentTurn="X";
    for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board.length; j++) {
            board[i][j] = "-";
        }
    }
}

public static void printBoard() {

    for (int i = 0; i < board.length; i++) {  
        for (int j = 0; j < board.length; j++) {
            if (j == 0) {
                System.out.print("| ");
            }
            System.out.print(board[i][j] + " | ");
        }
        System.out.println();
    }
}

public static void playerMove() {
    System.out.println();
    System.out.println("Your Move: ");
    row = scan.nextInt() ;
    column = scan.nextInt() ;
    while (board[row][column] != "-") {
    	System.out.println("Invalid entry. Please go again");
        row = scan.nextInt();
        column = scan.nextInt();
        
    }
    board[row][column] = getplayer();
    turn();
}


public static void computerMove() {
    Random computerMove = new Random();
    System.out.println();
    System.out.println("Computer Move: ");
        while (board[row][column] != "-") {
            row = computerMove.nextInt(3);
            column = computerMove.nextInt(3);
        }
        board[row][column] = getplayer();
        turn();
    }
	static String checkwin(){
		String line=null;
		for(int a=0;a<8;a++){
			switch (a){
				case 0:
					line=board[0][0]+board[0][1]+board[0][2];
					break;
				case 1:
					line=board[1][0]+board[1][1]+board[1][2];
					break;
				case 2:
					line=board[2][0]+board[2][1]+board[2][2];
					break;
				case 3:
					line=board[0][0]+board[1][0]+board[2][0];
					break;
				case 4:
					line=board[0][1]+board[1][1]+board[2][1];
					break;
				case 5:
					line=board[0][2]+board[1][2]+board[2][2];
					break;
				case 6:
					line=board[0][0]+board[1][1]+board[2][2];
					break;
				case 7:
					line=board[0][2]+board[1][1]+board[2][0];
					break;
				}
				if(line.equals("XXX")){
					return "X";
				}else if(line.equals("OOO")){
					return "O";
			}	
		}
		return null;
	}

public static String checkDraw() {
    for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board.length; j++) {
            if (board[i][j] == "-") {
                return null;
            }
        }
    }
    return "draw";
}
public static void playagain() {
	String ch;
        System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
        Scanner in =new Scanner(System.in);
        ch=in.nextLine();
        if((ch.equalsIgnoreCase("yes"))){
        	
        	initializeBoard();
            printBoard();
            
            win();
            System.out.println();
            winner();
            
        }
        else if (!(ch.equalsIgnoreCase("yes"))) {
            System.out.println("Bye!");
            System.exit(0);  // terminate the program
         }
}
}
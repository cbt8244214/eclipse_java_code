import java.util.Random;
import java.util.Scanner;

public class TicTacToe {
	static char[][] board=new char[3][3];
	static Scanner in = new Scanner(System.in);
	static char player;
	public static void main(String args[]){
		Start();	
	}
	public static void Start(){
		System.out.println("The Option\n 1.Player vs Player.\n 2.Player vs Computer.\nEnter :");
		while(true){
		int a=in.nextInt();
		if(a==1){
			initializeBoard();
	        win();
		}else if(a==2){
			initializeBoard();
			winningMove();
		}else
			System.out.println("Invalid Input. Go again :");
		}
	}
	 private static void initializeBoard(){
		 player='X';
			char a='1';
			for(int i=0;i<3;i++){
				for(int j=0;j<3;j++){
					board[i][j]=a;
					a++;
				}
			}
			printBoard();
		 }
	 private static void printBoard() {
			System.out.print((board[0][0]));
			System.out.print("|");
			System.out.print((board[0][1]));
			System.out.print("|");
			System.out.println((board[0][2]));
			System.out.println("-----");
			System.out.print((board[1][0]));
			System.out.print("|");
			System.out.print((board[1][1]));
			System.out.print("|");
			System.out.println((board[1][2]));
			System.out.println("-----");
			System.out.print((board[2][0]));
			System.out.print("|");
			System.out.print((board[2][1]));
			System.out.print("|");
			System.out.println((board[2][2]));
			}
	 public static  void win()
	    {
	        int spot;
	        char blank = ' ';
	        
	        System.out.println(  "Player " + getPlayer() +" will go first and be the letter 'X'" );
	        
	        do {
	           // currentBoard();              // display current board
	            
	            System.out.println(  "\n Player " + getPlayer() +" turn." );
	           
	            while (true) {
	         
	                spot=in.nextInt();
	                
	                if(validmove(spot)){
	                	break;
	                }else {
	                	System.out.println(spot + " is not a valid move.");
	            	}
	            }
	            placeMove( spot, getPlayer());
	            printBoard();             // display current board
	            
	            nextPlayer();
	        }while ( checkWinner() == blank );
	        playagain();
	    }
	 public static  char getPlayer()
	    {
	        return player;
	    } 
	  public static  void nextPlayer()
	    {
	        if (player == 'X')
	        player = 'O';
	        else player = 'X';
	        
	    }
	  public static  boolean validmove(int spot)
	    {
		  switch(spot) {
			case 1:
			return (board[0][0] == '1');
			case 2:
			return (board[0][1] == '2');
			case 3:
			return (board[0][2] == '3');
			case 4:
			return (board[1][0] == '4');
			case 5:
			return (board[1][1] == '5');
			case 6:
			return (board[1][2] == '6');
			case 7:
			return (board[2][0] == '7');
			case 8:
			return (board[2][1] == '8');
			case 9:
			return (board[2][2] == '9');
			default:
			return false;
			}
			
	        
	    }
	  public static  char checkWinner()
	    {
	        char Winner = ' ';
	        
	        // Check if X wins
	        if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') Winner = 'X';
	        if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X') Winner = 'X';
	        if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X') Winner = 'X';
	        if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X') Winner = 'X';
	        if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][0] == 'X' &&  board[1][1]  == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') Winner = 'X';
	        if (Winner == 'X' )
	        {System.out.println("Player-1 wins the game." );
	            return Winner;
	        }
	        
	        // Check if O wins
	        if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O') Winner = 'O';
	        if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O') Winner = 'O';
	        if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O') Winner = 'O';
	        if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O') Winner = 'O';
	        if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][0] == 'O' &&  board[1][1]  == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') Winner = 'O';
	        if (Winner == 'O' )
	        {
	            System.out.println( "Player-2 wins the game." );
	        return Winner; }
	        
	        // check for Tie
	        char a='1';
	        for(int i=0;i<board.length;i++){
	        	for(int j=0;j<board.length;j++){
	        		if(board[i][j]==a){
	        			return Winner;
	        		}
	        		a++;
	        	}
	        }
	        char Draw='D';
	        System.out.println(" Game is draw ");
	        return Draw;
	        
	    }

	  //---- ! ---- ! ----- ! ----- ! ----    ! ------ ! ------ ! --------.
	  private static void winningMove(){
			 while (true) {
					playerMove();
					printBoard();
					if (isGameFinished()){
						playagain();
					}
					
					computerMove();
					printBoard();
					if (isGameFinished()){
						playagain();
					}
					
				}
		 }
		 static void playerMove() {
				int userInput;
				while (true) {
				System.out.println("Player Move!!");
				userInput = in.nextInt();
				if (validmove( userInput)){
				break;
				} else {
				System.out.println(userInput + " is not a valid move.");
				}
				}
				placeMove( userInput, 'X');
				}
		
		 static void placeMove( int position, char symbol) {
				switch(position) {
				case 1:
				board[0][0] = symbol;
				break;
				case 2:
				board[0][1] = symbol;
				break;
				case 3:
				board[0][2] = symbol;
				break;
				case 4:
				board[1][0] = symbol;
				break;
				case 5:
				board[1][1] = symbol;
				break;
				case 6:
				board[1][2] = symbol;
				break;
				case 7:
				board[2][0] = symbol;
				break;
				case 8:
				board[2][1] = symbol;
				break;
				case 9:
				board[2][2] = symbol;
				break;
				}
				}
		 private static boolean isGameFinished() {
				if (hasContestantWon( 'X')) {
					//printBoard();
					System.out.println("Player wins!");
					return true;
				}
				if (hasContestantWon( 'O')) {
					//printBoard();
					System.out.println("Computer wins!");
					return true;
				}
				char n='1';
				for (int i = 0; i < board.length; i++) {
					for (int j = 0; j < board[i].length; j++) {
						if (board[i][j] == n) {
							return false;
						}
						n++;
					}
				}
				//printBoard();
				System.out.println("The game ended in a tie!");
				return true;
			}
		 private static boolean hasContestantWon( char symbol) {
				if ((board[0][0] == symbol && board [0][1] == symbol && board [0][2] == symbol) ||
				(board[1][0] == symbol && board [1][1] == symbol && board [1][2] == symbol) ||
				(board[2][0] == symbol && board [2][1] == symbol && board [2][2] == symbol) ||
				(board[0][0] == symbol && board [1][0] == symbol && board [2][0] == symbol) ||
				(board[0][1] == symbol && board [1][1] == symbol && board [2][1] == symbol) ||
				(board[0][2] == symbol && board [1][2] == symbol && board [2][2] == symbol) ||
				(board[0][0] == symbol && board [1][1] == symbol && board [2][2] == symbol) ||
				(board[0][2] == symbol && board [1][1] == symbol && board [2][0] == symbol) ) {
				return true;
				}
				return false;
				}
		 private static void computerMove() {
				Random rand = new Random();
				int computerMove;
				while (true) {
				computerMove = rand.nextInt(9) + 1;
				if (validmove(computerMove)) {
				break;
				}
				}
				System.out.println("Computer chose " + computerMove);
				placeMove(computerMove, 'O');
				}
		 public static void playagain() {
				String ch;
			        System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
			        Scanner in =new Scanner(System.in);
			        ch=in.nextLine();
			        if((ch.equalsIgnoreCase("yes"))){
			        	Start();
			        }
			        else if (!(ch.equalsIgnoreCase("yes"))) {
			            System.out.println("Bye!");
			            System.exit(0);  // terminate the program
			         }
			}
}

//87.Find out the number of days in between two given dates.
import java.util.Date;
//import java.text.ParseException;
import java.text.SimpleDateFormat;
public class NumberOfDays{
	public static void main(String[] args){
		SimpleDateFormat For=new SimpleDateFormat("dd-MM-yyyy");
		try{
			Date dateBefore=For.parse("31-01-2021");
			Date dateAfter=For.parse("31-01-2022");
			long difference=dateAfter.getTime()-dateBefore.getTime();	//1 second=1000 millisecond
			float daysBetween=(difference/(1000*60*60*24));	//millisecond in a day.
			System.out.println("Number of days="+daysBetween);//1000 millisecond*60 seconds* 60 minutes
		}													//24 hours*days
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
//84.write a program for singleton class.
//import java.util.*;
public class Singleton{
	private static Singleton myObj1;
	static{
		myObj1=new Singleton();
	}
	/*private Singleton(){ 	
	}*/
	public static Singleton getInstance(){
		return myObj1;
	}
	public void testMe(){
		System.out.println("hey");
	}
	public void testMe2(){
		System.out.println("hiii");
	}
	public static void main(String[] args){
		Singleton ms=getInstance();
		ms.testMe();
		ms.testMe2();
	}
}
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
public class CountWords {
	public static void main(String args[]){
		String s[]={"Learn","something","about","everything","and","everything","about","something"};
		LinkedHashMap <String,Integer> lhs=new LinkedHashMap<String,Integer>();
		for(String l:s){
			if(!(lhs.containsKey(l))){
				lhs.put(l, 1);
			}else
				lhs.put(l, lhs.get(l)+1);
		}
		System.out.println(lhs);
		Set<Map.Entry<String,Integer>>l=lhs.entrySet();
		for(Map.Entry<String, Integer>m:l){
			if(m.getValue()>=1){
				System.out.println(m.getKey()+"-"+m.getValue()+" times");
			}
		}
	}
}

import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Calendar;
public class DateofBirth1 {
	public static void main(String args[])throws Exception{
		Scanner s1=new Scanner(System.in);
		System.out.println("Enter the date of birth :");
		String s=s1.nextLine();
		s1.close();
		String pattern="yyyy-MM-DD";
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		 Calendar dob=Calendar.getInstance();
		 dob.setTime(sdf.parse(s));
		 System.out.println("Age is: "+getAge(dob));	 
	}
	public static int getAge(Calendar dob)throws Exception{
		Calendar today=Calendar.getInstance();
		int curyear =today.get(Calendar.YEAR);
		int dobyear=dob.get(Calendar.YEAR);
		int Age=curyear-dobyear;
		return Age;
	}
}

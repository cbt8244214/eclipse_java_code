import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
public class CountOcc {
	public static void main(String args[]){
		Scanner s1=new Scanner(System.in);
		System.out.print("Enter the character:");
		String s=s1.next();
		char[] c=s.toCharArray();
		LinkedHashMap <Character,Integer> lhs=new LinkedHashMap<Character,Integer>();
		for(char l:c){
			if(!(lhs.containsKey(l))){
				lhs.put(l, 1);
			}else
				lhs.put(l, lhs.get(l)+1);
		}
		System.out.println(lhs);
		Set<Map.Entry<Character,Integer>>l=lhs.entrySet();
		for(Map.Entry<Character, Integer>m:l){
			if(m.getValue()>=1){
				System.out.println(m.getKey()+"-"+m.getValue()+" times");
			}
		}
	}
}

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;


public class DateOfBirth3 {
	public static void main (String ars[]) throws Exception{
		Scanner s1=new Scanner(System.in);
		System.out.println("Enter the date of birth :");
		String s=s1.nextLine();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-DD");
		Calendar dob=Calendar.getInstance();
		dob.setTime(sdf.parse(s));
		System.out.println("Age is :"+getAge(dob));
	}
	public static int getAge(Calendar dob)throws Exception{
		Calendar today=Calendar.getInstance();
		int curyear=today.get(Calendar.YEAR);
		int dobyear=dob.get(Calendar.YEAR);
		int Age=curyear-dobyear;
		return Age;
	}
}

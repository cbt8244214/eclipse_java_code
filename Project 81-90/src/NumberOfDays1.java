//87.Find out the number of days in between two given dates.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
public class NumberOfDays1 {
	public static void main(String args[]){
		Scanner in=new Scanner(System.in);
		System.out.println("Enter BeforeDate :");
		String s=in.next();
		System.out.println("Enter AfterDate :");
		String s1=in.next();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		try{
			Date Beforedate=sdf.parse(s);
			Date Afterdate=sdf.parse(s1);
			long difference=Afterdate.getTime()-Beforedate.getTime();
			float betweendate=(difference/(1000*60*60*24));
			System.out.println(betweendate);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

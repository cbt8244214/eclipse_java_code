//81.COUNT THE NUMBER OF OCCURENCES OF EACH CHARACTER IN WORD
import java.util.Scanner;
public class NumberOfOccurence{
	public static void main(String[]args){
		System.out.println("Enter a word");
		Scanner sc=new Scanner(System.in);
		String s=sc.next();
		int c=0;
		for(char i='a';i<='z';i++){
			c=0;
			for(int j=0;j<s.length();j++){
				if(i==s.charAt(j)){
					c++;
				}
			}
			if(c>0){
				System.out.println(i+"-"+c+"times");
			}
		}
	}
}
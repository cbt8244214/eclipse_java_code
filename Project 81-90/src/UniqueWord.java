//85.Find the unique words from the paragraph.
import java.util.LinkedHashMap;
import java.util.Map;
public class UniqueWord{
	public static void main(String[] args){
		String str="guitar is instrument and piano is instrument";
		String strArray[]=str.split(" ");
		Map<String,String>hMap=new LinkedHashMap<String,String>();
		for(int i=0;i<strArray.length;i++){
			if(!hMap.containsKey(strArray[i])){
				hMap.put(strArray[i],"Unique");
			}
		} 
		System.out.println(hMap);
	}
} 
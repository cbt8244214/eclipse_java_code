//89.print the pattern
public class Pattern{
	public static void main(String[] args){
		int n=1;
		int l=4;
		for(int i=0;i<4;i++){
			for(int j=1;j<=n;j++){
				System.out.print("*");
			}
			System.out.println(" ");
			for(int k=1;k<=l;k++){
				if(l==16)break;
				System.out.print("*");
			}
			n=n+2;
			l=l+4;
			System.out.println(" ");
		}
	}
}
//89.print the pattern
public class Pattern1 {
	public static void main(String args[]){
		int n=1,l=4;
		for(int i=0;i<4;i++){
			for(int j=0;j<n;j++){
				System.out.print("*");
			}
			System.out.println();
			for(int k=0;k<l;k++){
				if(l==16)break;
				System.out.print("*");
			}
			System.out.println();
			n+=2;
			l+=4;
		}
	}
}

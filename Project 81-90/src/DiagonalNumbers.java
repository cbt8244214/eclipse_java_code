class DiagonalNumbers{
	static void printPrincipalDiagonal(int a[][],int n){
		System.out.print("PrincipalDiagonal: ");
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(i==j){
					System.out.print(a[i][j]+",");
				}
			}
		}
		System.out.println(" ");
	}
	static void printSecondaryDiagonal(int a[][],int n){
		System.out.print("SecondaryDiagonal: ");
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if((i+j)==(n-1)){
					System.out.print(a[i][j]+",");
				}
			}
		}
		System.out.println(" ");
	}
	public static void main(String args[]){
		int n=3;
		int a[][]={{1,2,3},{4,5,6},{7,8,9}};
		printPrincipalDiagonal(a,n);
		printSecondaryDiagonal(a,n);
	}
}
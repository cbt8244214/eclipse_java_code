//128.program to find highest prime factor for given number.
public class HighestPrimeFact{
	public static int highestpf(long num){
		int i;
		for( i=2;i<=num;i++){
			if(num%i==0){
				num/=i;
				i--;
			}
		}
		return i;
	}
	public static void main(String[]args){
		System.out.println(highestpf(35));
	}
}
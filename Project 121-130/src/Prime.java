//126.find the minimum number to add or substract to get the prime number.
import java.util.*;
public class Prime{
	
	static boolean meth(int a,int n){
		int num=a+n;
		boolean flag=false;
		for(int i=2;i<=num/2;i++){
			if(num%i==0){
				flag=true;
				break;
			}
		}
		return flag;
	}
	static boolean meth1(int a,int n1){
		int num=a+n1;
		boolean flag=false;
		for(int i=2;i<=num/2;i++){
			if(num%i==0){
				flag=true;
				break;
			}
		}
		return flag;
	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number :");
		int a=sc.nextInt();
		int n=0;
		int n1=0;
		for(int i=0;i<10;i++){
			if(!meth(a,n)||!meth1(a,n1)){
				if(!meth(a,n)){
					System.out.println(n);
					break;
				} 
				if(!meth1(a,n1)){
					System.out.println(n1);
					break;
				}
			}
			else{
				n++;
				n1--;
				meth(a,n);
				meth1(a,n1);
			}
		}
	}
}
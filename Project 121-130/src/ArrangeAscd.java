//121.write a program to arrange the left most bit by ascending order.
				//i/p=3,52,10,25,456   o/p= 10,25,3,456,52
import java.util.Arrays;
public class ArrangeAscd {
	public static void main(String args[]){
		int a[]={3,52,10,25,456};
		String s1[]=new String[a.length];
		for(int i=0;i<s1.length;i++){
			s1[i]=Integer.toString(a[i]);
		}
		Arrays.sort(s1);
		for(int i=0;i<s1.length;i++){
			a[i]=Integer.parseInt(s1[i]);
			System.out.print(a[i]+" ");
		}
	}
}

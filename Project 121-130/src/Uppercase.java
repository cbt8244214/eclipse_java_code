//122.Write a program to calculate the uppercase letters to lowercase and lower case to uppercase.
import java.util.Scanner;
public class Uppercase{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.print("enter first String :");
		String s=sc.nextLine();
		char c[]=s.toCharArray();
		for(int i=0;i<c.length;i++){
			if((c[i]>='A'&&c[i]<='Z')||(c[i]>='a'&&c[i]<='z')){
				if(c[i]>='A'&&c[i]<='Z')
					c[i]=(char)(c[i]+32);
				else if(c[i]>='a'&&c[i]<='z')
					c[i]=(char)(c[i]-32);
			}
		}
		System.out.print(c);
	}
}
//4.write a program to find whether the given number is odd or even without using condition statement.
import java.util.*;
public class OddorEven1 {
	public static void main(String args[]){
		Scanner in=new Scanner(System.in);
		System.out.println("Enter the Number :");
		int a=in.nextInt();
		int b=a%2;
		String c= b==0?"Even number":"Odd Number";
		System.out.println(c);
	}
}

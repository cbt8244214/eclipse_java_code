//18.find the second highest numbers in an array.
public class SecondHighest {
		public static void main(String args[]){
			int a[]={10,2,33,12,3,38,68,56};
			int b=a.length;
			int temp;
			for(int i=0;i<b;i++){
				for(int j=i+1;j<b;j++){
					if(a[i]>a[j]){
						temp=a[i];
						a[i]=a[j];
						a[j]=temp;
					}
				}
			}
			System.out.println("Second highest Number is :"+a[a.length-2]);
		}
	}


//16.write a program to remove duplicate numbers in an array.
import java.util.*;
public class DuplicateNumber {
	public static int remove(int a[],int n){
		if(n==0||n==1){
			return n;
		}
		int j=0;
		int b[]=new int[n];
		for(int i=0;i<n-1;i++){
			if(a[i]!=a[i+1]){
				b[j]=a[i];
				j++;
			}
		}
		b[j]=a[n-1];
		for(int i=0;i<j;i++){
			a[i]=b[i];
		}
		return j;
	}

	public static void main(String args[]){
		int a[]={10,22,34,23,22,34,56,74};
		Arrays.sort(a);
		int length=a.length;
		length=remove(a,length);
		for(int i=0;i<length;i++){
			System.out.print(a[i]+" ");
		}
	}
}

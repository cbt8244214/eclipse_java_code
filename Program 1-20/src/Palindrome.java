//19.check whether the given string is palindrome or not.if it is not palindrome,arrange the string in
//alphabetical order.
public class Palindrome {
	public static boolean ispalindrome(char[] s,int n){
		for(int i=0;i<n;i++){
			if(s[i]!=s[n-1]){
				return false;
			}
			n--;
		}
		return true;
	}
	public static void main(String args[]){
		String s="MAM";
		char c[]=s.toCharArray();
		int s1=s.length();
		boolean b=ispalindrome(c,s1);
		if(b)
			System.out.println("is a palindrome");
		else
			System.out.println("is not a palindrome");
	}
}

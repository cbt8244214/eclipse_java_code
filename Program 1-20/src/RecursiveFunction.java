//10.Write a program to find the sum of digits using recursive function. 
public class RecursiveFunction {
	public static void main(String args[]){
		int a=25631;
		int b,Sum=0;
		while(a>0){
			b=a%10;
			Sum+=b;
			a=a/10;
		}
		System.out.println(Sum);
	}
}

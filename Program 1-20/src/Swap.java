//6.Swap the two numbers without using temporary variable.
import java.util.*;
public class Swap {
	public static void main(String args[]){
		Scanner in=new Scanner(System.in);
		System.out.println("Enter the first Number :");
		int a=in.nextInt();
		System.out.println("Enter the Second Number :");
		int b=in.nextInt();
		System.out.println("Before the swap:");
		System.out.println("a= "+a+" ,b="+b);
		a=a+b;		//11
		b=a-b;		//11-6=>5
		a=a-b;		//11-5 =>6
		System.out.println("After the Swap :");
		System.out.println("a= "+a+" ,b="+b);
	}
}

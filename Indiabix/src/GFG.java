 public class GFG {
 
    // Function to return the resultant string
    static String updateString(String S, String A, String B)
    {
 
        int l = A.length();
 
        // Iterate through all positions i
        for (int i = 0; i + l <= S.length(); i++) {
 
            // Current sub-string of length = len(A) = len(B)
            String curr = S.substring(i, i + l);
 
            // If current sub-string gets equal to A or B
            if (curr.equals(A)) {
 
                // Update S after replacing A
                String new_string
                    = S.substring(0, i)
                      + B + S.substring(i + l, S.length());
                S = new_string;
                i += l - 1;
            }
            else {
 
                // Update S after replacing B
                String new_string
                    = S.substring(0, i)
                      + A + S.substring(i + l, S.length());
                S = new_string;
                i += l - 1;
            }
        }
 
        // Return the updated string
        return S;
    }
 
    // Driver code
    public static void main(String[] args)
    {
        String S = "aab";
        String A = "aa";
        String B = "bb";
 
        System.out.println(updateString(S, A, B));
    }
}
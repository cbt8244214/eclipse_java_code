//69.print the spiral matrix.
import java.util.*;
class Spiral{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of elements:");
		int n=sc.nextInt();
		int A[][]=new int[n][n];
		int k=1,c1=0,c2=3,r1=0,r2=3;
		for(int i1=16;i1>=1;i1--){
			for(int i=c1;i<=c2;i++){
				A[c1][i]=k++;
			}
			for(int j=r1+1;j<=r2;j++){
				A[j][r2]=k++;
			}
			for(int i=c2-1;i>=c1;i--){
				A[c2][i]=k++;
			}
			for(int j=r2-1;j>=r1+1;j--){
				A[j][r1]=k++;
			}
			c1++;
			c2--;
			r1++;
			r2--;
		}
/*PrintingtheCircularmatrix*/
		System.out.println("The Circular Matrix is:");
			for(int i=0;i<n;i++){
				for(int j=0;j<n;j++){
					System.out.print(A[i][j]+"\t");
				}
				System.out.println();
			}
	}
}
//67.print the pattern.
public class Pattern2{
	public static void main(String[] args){
		int i,j,k,s=2,c=2,p=1;
		for(i=1;i<=4;i++){
			for(j=7;j>=2*i;j--){
				System.out.print(" ");
			}
			for(k=1;k<=i*2-1;k++){
				if(k==i){
					System.out.print(p+" ");
					p=0; 
					c--;
				}else{
					System.out.print(c+" ");
				}
				p=p+s*3;
				s++;
				c++;
			}
			System.out.println();
		}
	}
}
//63.print the diamond pattern.
public class Diamond1 {
	public static void main(String args[]){
		//int n=5;
		for(int i=1;i<=3;i++){
			for(int j=1;j<=3-i;j++){
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++){
				System.out.print("* ");
			}
			System.out.println();
		}
		for(int a=3;a>1;a--){
			for(int b=1;b<=3-a;b++){
				System.out.print(" ");
			}
			for(int c=1;c<a;c++){
				System.out.print(" *");
			}
			System.out.println();
		}
	}
}

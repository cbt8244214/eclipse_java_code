//63.print the diamond matrix.
public class Diamond {
	public static void main (String args[]){
		int n=5;
		for(int i=1;i<=(n+1)/2;i++){
			for(int j=1;j<=(n+1)/2-i;j++){
				System.out.print(" ");
			}
			for(int r=1;r<=i;r++){
				System.out.print("* ");
			}
			System.out.println();
		}
		for(int a=(n+1)/2;a>1;a--){
			for(int b=1;b<=(n+1)/2-a;b++){
				System.out.print(" ");
			}
			for(int i1=1;i1<a;i1++){
				System.out.print(" *");
			}
			System.out.println();
		}
	}
}

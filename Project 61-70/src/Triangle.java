//65.print the number pattern.
public class Triangle{
	public static void main(String args[]){
		int n=1;
		for(int i=0;i<3;i++){
			for(int k=4;k>2*i;k--){
				System.out.print(" ");
			}
			for(int j=0;j<2*i+1;j++){
				if(j==i||j==0){
					if(j==i){
						System.out.print(" "+(n-i));
					}else
						System.out.print(" "+(n+i));
				}
				else{
					System.out.print(" "+n);
				}
				n++;
			}
			System.out.println();
		}
	}
}
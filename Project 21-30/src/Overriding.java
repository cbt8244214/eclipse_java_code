//28.write a program to implement overriding concept. 
class A1{
	void meth(){
		System.out.println("Hello");
	}
}
class B1 extends A1{
	void meth(){
		System.out.println("Hi");
	}
}
public class Overriding {
	public static void main(String args[]){
		B1 b=new B1();
		b.meth();
	}
}

//25.Array={1,2,5,6,9,7},Array2={3,4,12,10}.Merge Array1,Array2 and sort the numbers.
import java.util.Arrays;
public class MergeArray {
	static int[] mergeArray(int[]array1,int[]array2){
	int[] mergedArray=new int[array1.length+array2.length];
	int i=0,j=0,k=0;
	while(i<array1.length){
		mergedArray[k]=array1[i];
		i++;
		k++;
	}
	while(j<array2.length){
		mergedArray[k]=array2[j];
		j++;
		k++;
	}
	Arrays.sort(mergedArray);
	return mergedArray;
}
public static void main(String args[]){
	int[]array1=new int[]{1,2,5,6,9,7};
	int[]array2=new int[]{3,4,12,10};
	int[]mergedArray=mergeArray(array1,array2);
	System.out.println("Array1:"+Arrays.toString(array1));
	System.out.println("Array2:"+Arrays.toString(array2));
	System.out.println("Merged Array:"+Arrays.toString(mergedArray));
}
}

//29.write a program to implement abstract class concept.
abstract class A10 {
	abstract void callme();
}
class B10 extends A10{
	void callme(){
		System.out.println("B10's implement of callme");
	}
}
class Abstractdemo{
	public static void main(String args[]){
		B10 b=new B10();
		A10 a;
		a=b;
		a.callme();
	}
}

//27.write a program to implement inheritance concept.
class A{
	int a;
	int meth(){
		return a;
	}
}
class B extends A{
	int b;
	int show(){
		return b;
	}
}
public class Inheritance {
	public static void main(String args[]){
		A a1=new A();
		B b1=new B();;
		b1.a=1;
		b1.b=2;
		System.out.println(b1.show());
		System.out.println(b1.meth());
	}
}

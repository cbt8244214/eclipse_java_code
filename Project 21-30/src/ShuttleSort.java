//24.write a program to sort the numbers in descending order using shuttle sort algorithm.
import java.util.Arrays;
public class ShuttleSort {
	public static void main(String args[]){
		int array[]={10,21,16,3,1,25,7};
		System.out.println(Arrays.toString(array));
		for(int i=1;i<array.length;i++){
			if(array[i]>array[i-1]){
				int temp=array[i];
				array[i]=array[i-1];
				array[i-1]= temp;
				for(int z=i-1;(z-1)>=0;z--){
					if(array[z]>array[z-1]){
						temp=array[z];
						array[z]=array[z-1];
						array[z-1]=temp;
					}
					else{
						break;
					}
				}
			}
		}
		System.out.println(Arrays.toString(array));
	}
}

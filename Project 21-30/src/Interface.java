//30.write a program to implement interface concept. 
interface A2{
	 void meth1();
 }
 interface B2 extends A2{
	 void meth2();
 }
 interface C2 extends B2{
	 void meth3();
 }
 class Myclass implements C2{
	public void meth1(){
		 System.out.println("Hi");
	 }
	public void meth2(){
		 System.out.println("Hello");
	 }
	public void meth3(){
		 System.out.println("Hey");
	 }
 }
public class Interface {
	public static void main(String args[]){
		Myclass ab=new Myclass();
		ab.meth3();
		ab.meth1();
		ab.meth2();
	}
}

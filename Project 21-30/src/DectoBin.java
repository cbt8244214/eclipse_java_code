//22.Convert Decimal to Binary Format.
import java.util.Scanner;										
public class DectoBin {
	public static void main(String args[]){
		int dec,n=0;												//in this program we can also execute
		Scanner s=new Scanner(System.in);							//by using collection concept.
		System.out.println("Enter the decimal number");
		dec=s.nextInt();
		String bin =Integer.toBinaryString(dec);
		System.out.println(dec+" in decimal="+bin+" in binary.");
		while(dec>=1){											//decimal number is 17
		  	n=dec%2;												//17/2=>q=8,r=1;
			System.out.print(n);									//8/2=>q=4,r=0;
			dec=dec/2;	
		}													//4/2=>q=2,r=0;
	}															//2/2=>q=1,r=0;
}												//then the reminder value is binary=>10001
														
			
		

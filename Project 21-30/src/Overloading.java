//26.write a program to implement overloading concept.
class Overload {
	void meth(){
		System.out.println("Hello");
	}
	void meth(int a,int b){
		System.out.println("a and b: "+a+", "+b);
	}
}
public class Overloading {
	public static void main(String args[]){
		Overload O =new Overload();
		O.meth();
		O.meth(15, 5);
	}
}

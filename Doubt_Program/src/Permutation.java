//77.write a program to find all permutation of the string.
import java.util.Scanner;
public class Permutation{
	static void permute(String s,String ans){
		if(s.length()==0){
			System.out.print(ans+" ");
			return;
		}
		for(int i=0;i<s.length();i++){
			char ch=s.charAt(i);
			String ros=s.substring(0, i)+s.substring(i+1);
			permute(ros,ans+ch);
		}
	}
	public static void main(String args[]){
		String s;
		String ans="";
		Scanner s1=new Scanner(System.in);
		System.out.print("Enter the string");
		s=s1.next();
		System.out.println("All possible Strings are :");
		permute(s,ans);
	}
}
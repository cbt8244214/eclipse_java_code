//129.Write a program to shuffle the digits in the given number and find the highest number.
public class HighestNumber{
	public static void main(String args[]){
		int num=123;
		int result=0,mul=1;
		int count[]=new int[10];
		String str=String.valueOf(num);
		for(int i=0;i<str.length();i++){
			count[str.charAt(i)-'0']++;
		}
		for(int i=0;i<=9;i++){
			while(count[i]>0){
				result=result+(i*mul);
				count[i]--;
				mul=mul*10;
			}
		}
		System.out.print(result);
	}
}
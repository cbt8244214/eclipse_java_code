import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Calendar;
public class DateOfBirth{
	public static void main(String args[])throws Exception{
		System.out.print("enter date d birth");
		Scanner s=new Scanner(System.in);
		String input=s.nextLine();
		s.close();
		String pattern="yyyy-MM-DD";
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		Calendar dob=Calendar.getInstance();
		dob.setTime(sdf.parse(input));
		System.out.println("age is:"+getAge(dob));
	}
	public static int getAge(Calendar dob)throws Exception{
		Calendar today=Calendar.getInstance();
		int curyear=today.get(Calendar.YEAR);
		int dobYear=dob.get(Calendar.YEAR);
		int age=curyear-dobYear;
		return age;
	}
}
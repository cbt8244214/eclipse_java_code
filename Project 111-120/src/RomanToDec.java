//119.write a program to convert Roman digit to Decimal number.
import java.util.*;
public class RomanToDec{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter roman letter");
		String s=sc.next();
		Map<Character,Integer>m=new LinkedHashMap<Character,Integer>();
		m.put('I',1);
		m.put('V',5);
		m.put('X',10);
		m.put('L',50);
		m.put('C',100);
		m.put('D',500);
		m.put('M',1000);
		if(s.length()>2){
			int temp=m.get(s.charAt(s.length()-1));
			for(int i=0;i<s.length()-1;i++){
				if(m.get(s.charAt(i))>=m.get(s.charAt(i+1))){
					temp=temp+m.get(s.charAt(i));
				}
				else{
					temp=temp-m.get(s.charAt(i));
				}
			}
			System.out.println(temp);
		}
		else if(s.length()==2){
			int temp=0;
			for(int i=0;i<s.length()-1;i++){
				if(m.get(s.charAt(i))>=m.get(s.charAt(i+1))){
					temp=temp+m.get(s.charAt(i));
				}
				else{
					temp=temp-m.get(s.charAt(i));
				}
				temp=temp+m.get(s.charAt(s.length()-1));
			}
			System.out.println(temp);
		}
		else{
			int temp=m.get(s.charAt(0));
			System.out.println(temp);
		}
	}
}
//114.write a program to sort even and odd numbers in ascending order.
import java.util.Arrays;
public class Arrays1{
	public static void main(String[] args){
		int a[]={2,8,4,9,3,6,1,7};
		Arrays.sort(a);		//1,2,3,4,6,7,8,9.
		for(int i=0;i<a.length;i++){
			if(a[i]%2==0)
				System.out.print(a[i]);
		}
		for(int i=0;i<a.length;i++){
			if(a[i]%2!=0)
				System.out.print(a[i]);
		}
	}
}

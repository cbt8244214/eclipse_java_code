//115.Add 1 number while the given number is even and substract 1 number while the given num. is odd.
			/*i/p=2 3 1 6 7
			 o/p= 3 2 0 7 6 */
public class AddorSub{
	public static void main(String args[]){
		int a[]={2,3,1,6,7};
		for(int i=0;i<5;i++){
			if(a[i]%2==0){
				System.out.print(a[i]+1);
			}
			else System.out.print(a[i]-1);
		}
	}
}
/* a[]={2,3,1,6,7};
	for(int i=0;i<5;i++){
	if(a[i]%2==0){
	System.out.print(a[i]+1);
	}else
	System.out.print(a[i]-1);
	}*/
//117.write a program to find the first repeating character in a given string using a single iteration.
public class FirstRepeat{
	public static void main(String[] args){
		String str="ICECREAM";
		int k=0;
		//char a[]=str.toCharArray();
		for(int i=0;i<str.length();i++){
			for(int j=i+1;j<str.length();j++){
				if(str.charAt(i)==str.charAt(j)){
					k++;
					break;
				}
			}
			if(k==1){
				System.out.println(str.charAt(i));
				break;
			}
		}
	}
}
//120.write a program to find the last non repeated character.
//import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Map.Entry;
public class NonRepeatedChar{
	public static void main(String args[]){
		String inp="BOOK";
		LinkedHashMap<Character,Integer>count=new LinkedHashMap<Character,Integer>();
		char[] strArray=inp.toCharArray();
		for(char c:strArray){
			if(count.containsKey(c)){
				count.put(c,count.get(c)+1);
			}
			else{
				count.put(c,1);
			}
		}
		/*for(char c:strArray){
			if(count.get(c)==1){
				System.out.println("Non repeated character :"+c);
				break;
			}
		}*/
		Character a=' ';
		Set<Entry<Character,Integer>>c1=count.entrySet();
		for(Entry<Character,Integer>c:c1){
			if(c.getValue()==1){
				a = c.getKey();
			}
		}
		System.out.println("Last Non repeated character :"+a);
	}
}
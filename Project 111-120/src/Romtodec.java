import java.util.LinkedHashMap;
import java.util.Scanner;


public class Romtodec {
	public static void main(String args[]){
		LinkedHashMap <Character,Integer> lhm=new LinkedHashMap <Character,Integer>();
		lhm.put('I', 1);
		lhm.put('V', 5);
		lhm.put('X', 10);
		lhm.put('L', 50);
		lhm.put('C', 100);
		lhm.put('D', 500);
		lhm.put('M', 1000);
		Scanner sc=new Scanner(System.in);
		System.out.println("enter roman letter");
		String s=sc.next();
		if(s.length()>2){
			int temp=lhm.get(s.charAt(s.length()-1));
			for(int i=0;i<s.length()-1;i++){
				if(lhm.get(s.charAt(i))>=lhm.get(s.charAt(i+1))){
					temp=temp+lhm.get(s.charAt(i));
				}else
					temp=temp-lhm.get(s.charAt(i));
			}
			System.out.println(temp);
		}
		else if(s.length()==2){
			int temp=0;
			for(int i=0;i<s.length()-1;i++){
				if(lhm.get(s.charAt(i))>=lhm.get(s.charAt(i+1))){
					temp=temp+lhm.get(s.charAt(i));
				}else{
					temp=temp-lhm.get(s.charAt(i));
				}
				temp=temp+lhm.get(s.charAt(i));
			}
			System.out.println(temp);
		}else{
			int temp=lhm.get(s.charAt(0));
			System.out.println(temp);
		}
	}
}

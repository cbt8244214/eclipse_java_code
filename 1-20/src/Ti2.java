import java.util.Random;
import java.util.Scanner;


public class Ti2 {
	static char[][] Board=new char[3][3];
	static Scanner in=new Scanner(System.in);
	public static void main(String args[]){
		initiallizeBoard();
		win();
	}
	public static void initiallizeBoard(){
		 char a='1';
		 for(int i=0;i<Board.length;i++){
			 for(int j=0;j<Board.length;j++){
				 Board[i][j]=a;
				 a++;
			 }
		 }
		 printBoard();
	}
	public static void printBoard(){
		System.out.println("| "+Board[0][0]+" | "+Board[0][1]+" | "+Board[0][2]+" |");
		System.out.println("|---|---|---|");
		System.out.println("| "+Board[1][0]+" | "+Board[1][1]+" | "+Board[1][2]+" |");
		System.out.println("|---|---|---|");
		System.out.println("| "+Board[2][0]+" | "+Board[2][1]+" | "+Board[2][2]+" |");
	}
	public static void win(){
		while(true){
		playerMove();
		if(gamefinished()){
			playagain();
		}
		printBoard();
		computerMove();
		if(gamefinished()){
			playagain();
		}
		printBoard();
		}
	}
	public static void playerMove(){
		int spot;
		while(true){
			System.out.println(" X turn");
			spot=in.nextInt();
			if(validMove(spot)){
				break;
			}else{
				System.out.println("Invalid input. Go again");
			}
			
		}
		placeMove(spot,'X');
	
	}
	public static void computerMove(){
		Random rand=new Random();
		int compt;
		while(true){
			compt=rand.nextInt(9)+1;
			if(validMove(compt)){
				break;
			}
		}			
		System.out.println("Computer choose "+compt);
		placeMove(compt,'O');
		
	}
	public static boolean validMove(int position){
		switch(position){
		case 1:
			return (Board[0][0]=='1');	
		case 2:
			return (Board[0][1]=='2');
		case 3:
			return (Board[0][2]=='3');
		case 4:
			return (Board[1][0]=='4');
		case 5:
			return (Board[1][1]=='5');
		case 6:
			return (Board[1][2]=='6');
		case 7:
			return (Board[2][0]=='7');
		case 8:
			return (Board[2][1]=='8');
		case 9:
			return (Board[2][2]=='9');
			default:
			return false;
		}	
	}
	public static void placeMove(int  position,char symbol){
		switch(position) {
		case 1:
		Board[0][0] = symbol;
		break;
		case 2:
		Board[0][1] = symbol;
		break;
		case 3:
		Board[0][2] = symbol;
		break;
		case 4:
		Board[1][0] = symbol;
		break;
		case 5:
		Board[1][1] = symbol;
		break;
		case 6:
		Board[1][2] = symbol;
		break;
		case 7:
		Board[2][0] = symbol;
		break;
		case 8:
		Board[2][1] = symbol;
		break;
		case 9:
		Board[2][2] = symbol;
		break;
		}
	}
	public static boolean gamefinished(){
		if(contestantwon('X')){
			printBoard();
			System.out.println(" player win");
			return true;
		}
		if(contestantwon('O')){
			printBoard();
			System.out.println(" Computer win");
			return true;
		}
		char a='1';
		for(int i=0;i<Board.length;i++){
			for(int j=0;j<Board.length;j++){
				if(Board[i][j]==a){
					return false;
				}
				a++;
			}
		}
		printBoard();
		System.out.println(" Match draw");
		return true;
	}
	public static boolean contestantwon(char symbol){
	
		if ((Board[0][0] == symbol && Board[0][1] == symbol && Board[0][2] == symbol) ||
         (Board[1][0] == symbol && Board[1][1] == symbol && Board[1][2] == symbol) ||
         (Board[2][0] == symbol && Board[2][1] == symbol && Board[2][2] == symbol) ||
         (Board[0][0] == symbol && Board[1][0] == symbol && Board[2][0] == symbol) ||
        (Board[0][1] == symbol && Board[1][1] == symbol && Board[2][1] == symbol) ||
        (Board[0][2] == symbol && Board[1][2] == symbol && Board[2][2] == symbol) ||
        (Board[0][0] == symbol && Board[1][1] == symbol && Board[2][2] == symbol) ||
        (Board[0][2] ==symbol && Board[1][1] == symbol && Board[2][0] == symbol)) 
		{
        return true;
		}
		return false;
	}
	public static void playagain(){
		 System.out.println("would you like to play again ( enter 'yes') or enter to stop :");
			Scanner in=new Scanner(System.in);
			 String ch=in.nextLine();
			 if((ch.equalsIgnoreCase("yes"))){
				 initiallizeBoard();
					win();
			 }
			 else if(!(ch.equalsIgnoreCase("yes"))){
				 System.out.println("Bye");
				 System.exit(0);
			 }
	}
}

import java.util.Scanner;
public class Ti1 {
	static Scanner in=new Scanner(System.in);
	static char[][] Board=new char[3][3];
	static char Player;
	public static void main(String args[]){
		initiallizeBoard();
		win();
	}
	public static void initiallizeBoard(){
		Player='X';
		 char a='1';
		 for(int i=0;i<Board.length;i++){
			 for(int j=0;j<Board.length;j++){
				 Board[i][j]=a;
				 a++;
			 }
		 }
		 printBoard();
	}
	public static void printBoard(){
		System.out.println("| "+Board[0][0]+" | "+Board[0][1]+" | "+Board[0][2]+" |");
		System.out.println("|---|---|---|");
		System.out.println("| "+Board[1][0]+" | "+Board[1][1]+" | "+Board[1][2]+" |");
		System.out.println("|---|---|---|");
		System.out.println("| "+Board[2][0]+" | "+Board[2][1]+" | "+Board[2][2]+" |");
	}
	public static void win(){
		int spot;
		System.out.println(getPlayer()+" will start first");
		do{
			System.out.println(getPlayer()+" turn");
			while(true){
				
				spot=in.nextInt();
				if(ValidMove(spot)){
					break;	
				}else
					System.out.println("Invalid input . Go again");
			}
			placeMove(spot,getPlayer());
			printBoard();
			
			nextPlayer();
		}while(checkWinner()==' ');
		playagain();
	}
	public static char getPlayer(){
		//Player='X';
		return Player;
	}
	public static void nextPlayer(){
		if(Player=='X')
			Player='O';
		else
			Player='X';
		
	}
	public static boolean ValidMove(int spot){
		switch(spot){
		case 1:
			return (Board[0][0]=='1');	
		case 2:
			return (Board[0][1]=='2');
		case 3:
			return (Board[0][2]=='3');
		case 4:
			return (Board[1][0]=='4');
		case 5:
			return (Board[1][1]=='5');
		case 6:
			return (Board[1][2]=='6');
		case 7:
			return (Board[2][0]=='7');
		case 8:
			return (Board[2][1]=='8');
		case 9:
			return (Board[2][2]=='9');
			default:
			return false;
		}
		
	}
	public static void placeMove(int  position,char symbol){
		switch(position) {
		case 1:
		Board[0][0] = symbol;
		break;
		case 2:
		Board[0][1] = symbol;
		break;
		case 3:
		Board[0][2] = symbol;
		break;
		case 4:
		Board[1][0] = symbol;
		break;
		case 5:
		Board[1][1] = symbol;
		break;
		case 6:
		Board[1][2] = symbol;
		break;
		case 7:
		Board[2][0] = symbol;
		break;
		case 8:
		Board[2][1] = symbol;
		break;
		case 9:
		Board[2][2] = symbol;
		break;
		}
	}
	public static char checkWinner(){
		 char Winner = ' ';
	        
	        // Check if X wins
	        if (Board[0][0] == 'X' && Board[0][1] == 'X' && Board[0][2] == 'X') Winner = 'X';
	        if (Board[1][0] == 'X' && Board[1][1] == 'X' && Board[1][2] == 'X') Winner = 'X';
	        if (Board[2][0] == 'X' && Board[2][1] == 'X' && Board[2][2] == 'X') Winner = 'X';
	        if (Board[0][0] == 'X' && Board[1][0] == 'X' && Board[2][0] == 'X') Winner = 'X';
	        if (Board[0][1] == 'X' && Board[1][1] == 'X' && Board[2][1] == 'X') Winner = 'X';
	        if (Board[0][2] == 'X' && Board[1][2] == 'X' && Board[2][2] == 'X') Winner = 'X';
	        if (Board[0][0] == 'X' && Board[1][1] == 'X' && Board[2][2] == 'X') Winner = 'X';
	        if (Board[0][2] == 'X' && Board[1][1] == 'X' && Board[2][0] == 'X') Winner = 'X';
	        if (Winner == 'X' )
	        {System.out.println("Player-1 wins the game." );
	            return Winner;
	        }
	        
	        // Check if O wins
	        if (Board[0][0] == 'O' && Board[0][1] == 'O' && Board[0][2] == 'O') Winner = 'O';
	        if (Board[1][0] == 'O' && Board[1][1] == 'O' && Board[1][2] == 'O') Winner = 'O';
	        if (Board[2][0] == 'O' && Board[2][1] == 'O' && Board[2][2] == 'O') Winner = 'O';
	        if (Board[0][0] == 'O' && Board[1][0] == 'O' && Board[2][0] == 'O') Winner = 'O';
	        if (Board[0][1] == 'O' && Board[1][1] == 'O' && Board[2][1] == 'O') Winner = 'O';
	        if (Board[0][2] == 'O' && Board[1][2] == 'O' && Board[2][2] == 'O') Winner = 'O';
	        if (Board[0][0] == 'O' && Board[1][1] == 'O' && Board[2][2] == 'O') Winner = 'O';
	        if (Board[0][2] == 'O' && Board[1][1] == 'O' && Board[2][0] == 'O') Winner = 'O';
	        if (Winner == 'O' )
	        {
	            System.out.println( "Player-2 wins the game." );
	        return Winner; }
	        
	        // check for Tie
	        char a='1';
	        for(int i=0;i<Board.length;i++){
	        	for(int j=0;j<Board.length;j++){
	        		if(Board[i][j]==a){
	        			return Winner;
	        		}
	        		a++;
	        	}
	        }
	        char Draw='D';
	        System.out.println(" Game is draw ");
	        return Draw;
	        
	}
	 public static void playagain() {
			String ch;
		        System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
		        //Scanner in =new Scanner(System.in);
		        ch=in.nextLine();
		        if((ch.equalsIgnoreCase("yes"))){
		        	initiallizeBoard();
		        	win();
		        }
		        else if (!(ch.equalsIgnoreCase("yes"))) {
		            System.out.println("Bye!");
		            System.exit(0);  // terminate the program
		         }
		}
}

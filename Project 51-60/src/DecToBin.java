//51.CONVERT DECIMAL TO BINARY USING STACK.
import java.util.Stack;
import java.util.Scanner;
class DecToBin{
	public static void main(String args[]){
		int n;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the decimal number : ");
		n=s.nextInt();
		int r;
		Stack <Integer>stk=new Stack <Integer> ();
			while(n!=0)
			{
				r=n%2;
				stk.push(r);
				n=n/2;
			}
			System.out.print("binary:");
			while(!(stk.isEmpty()))
			{
				System.out.print(stk.pop());
			}
	}
}	
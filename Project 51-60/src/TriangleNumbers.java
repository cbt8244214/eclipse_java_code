//60.print the matrix.
public class TriangleNumbers{
	public static void main(String[] args){
		int n=4;
		for(int i=1;i<=n;i++){
			for(int j=n;j>i;j--){
				System.out.print(" ");
			}
			for(int k=i-1;k>-i;k--){
				System.out.print(i-Math.abs(k));
			}
				System.out.println();
		}
	}
}

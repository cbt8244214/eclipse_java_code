
public class MultipleCatch {
   public static void main(String[] args) {
		try{
	int a=args.length;
	System.out.println("a="+a);
	int b=5/a;
	int c[]={10};
	c[1]=20;
	}
		catch(ArithmeticException e){
		System.out.println("Divide by 0"+e);
		}
		catch(ArrayIndexOutOfBoundsException e){
		System.out.println("Array index oob"+e);
		}
		System.out.println("After try/catch blocks");
}
}
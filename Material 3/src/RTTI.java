class A{
	int i;
}
class B extends A{
	int j;
}
public class RTTI {
	public static void main(String args[]){
		A a=new A();
		B b=new B();
		Class c;
		c=a.getClass();
		System.out.println("c1Obj is:"+c);
		System.out.println("a is object of type:"+c.getName());
		c=b.getClass();
		System.out.println("c1Obj is:"+c);
		System.out.println("b is object of type:"+c.getName());
		c=c.getSuperclass();
		System.out.println("c1Obj is :"+c);
		System.out.println("b's superclass is:"+c.getName());
	}
}

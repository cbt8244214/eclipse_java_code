class MemoryManagementDemo {
public static void main(String[] args) {
Runtime rt=Runtime.getRuntime();
long mem1,mem2;
Integer IntObj[]=new Integer[100];
System.out.println("Total memory is:"+rt.totalMemory());
mem1=rt.freeMemory();
System.out.println("Initial free memory:"+mem1);
rt.gc();
mem1=rt.freeMemory();
System.out.println("Free memory after garbage collection:"+mem1);
for(int i=0;i<100;i++)
	IntObj[i]=new Integer(i);
mem2=rt.freeMemory();
System.out.println("Free memory after allocation:"+mem2);
System.out.println("Memory used by allocation:"+(mem1-mem2));
for(int i=0;i<100;i++)IntObj[i]=null;
rt.gc();
mem2=rt.freeMemory();
System.out.println("Free memory after collecting discarded integers:"+mem2);
}
}
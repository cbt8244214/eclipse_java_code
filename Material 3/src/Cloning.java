public class Cloning implements Cloneable {
	int a;
	Cloning cloning(){
		try{
			return(Cloning)super.clone();
		}catch(CloneNotSupportedException e){
			System.out.println("Cloning not allowed");
			return this;
		}
	}
}
class CloneDemo{
	public static void main(String args[]){
		Cloning c1=new Cloning();
		Cloning c2;
		c1.a=5;
		System.out.println("c1.a:"+c1.a);
		c2=c1.cloning();
		c1.a=10;
		System.out.println("c1.a:"+c1.a);
		System.out.println("c2.a:"+c2.a);
	}
}

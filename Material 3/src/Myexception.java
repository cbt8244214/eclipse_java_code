class Myexception extends Exception {
	private int detail;
Myexception(int a){
	detail=a;
}
public String toString(){
	return "MyException["+detail+"]";
}
}
class ExceptionDemo{
	static void compute(int a)throws Myexception{
		System.out.println("called compute("+a+")");
		if(a>5)
			throw new Myexception(a);
		System.out.println("Normal exit");
	}
	public static void main(String args[]){
try{
	compute(1);
	compute(10);
}
catch(Myexception e){
	System.out.println("caught"+e);
}
}
}
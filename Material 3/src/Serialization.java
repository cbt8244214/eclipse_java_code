import java.io.*;
public class Serialization {
	public static void main(String args[]){
		try{
			MyClass object1=new MyClass("java",5);
			System.out.println("object1:"+object1);
			FileOutputStream fos=new FileOutputStream("serial");
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(object1);
			oos.flush();
			oos.close();
		}catch(IOException e){
			System.out.println("Exception during serialization"+e);
			System.exit(0);
		}
		try{
			MyClass object2;
			FileInputStream fis=new FileInputStream("Serial");
			ObjectInputStream ois=new ObjectInputStream(fis);
			object2=(MyClass)ois.readObject();
			ois.close();
			System.out.println("object2:"+object2);
		}catch(Exception e){
			System.out.println("Exception during deserializatrion:"+e);
			System.exit(0);
		}
	}
}
class MyClass implements Serializable{
	String s;
	transient int i;
	public MyClass(String s,int i){
		this.s=s;
		this.i=i;
	}
	public String toString(){
		return"s="+s+";i="+i;
	}
}

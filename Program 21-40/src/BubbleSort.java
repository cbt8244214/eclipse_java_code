//23.write a program to sort the number in ascending order using bubble sort algorithm.
import java.util.*;
public class BubbleSort {
	public static void main(String args[]){
		int a[]={10,8,12,46,78};
		System.out.println(Arrays.toString(a));
		int n=a.length;
		int temp;
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				if(a[i]>a[j]){
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		/*for(int i=0;i<n;i++){
			System.out.print(a[i]+" ");*/
		System.out.println(Arrays.toString(a));
	}
}

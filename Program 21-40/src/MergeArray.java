//25.merge the array sort the numbers.
import java.util.*;
public class MergeArray {
	public static void main(String args[]){
		int a1[]={1,2,3,6,9,7};
		int a2[]={3,4,12,10};
		int a3[]=new int[a1.length+a2.length];
		int i=0,j=0,k=0;
		while(i<a1.length){
			a3[k]=a1[i];
			k++;
			i++;
		}
		while(j<a2.length){
			a3[k]=a2[j];
			j++;
			k++;
		}
		System.out.println(Arrays.toString(a1));
		System.out.println(Arrays.toString(a2));
		Arrays.sort(a3);
		System.out.println(Arrays.toString(a3));
	}
}

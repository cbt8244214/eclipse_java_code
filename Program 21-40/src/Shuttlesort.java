//24.write a program to sort the numbers in descending order using shuttle sort algorithm.
import java.util.*;
public class Shuttlesort {
	public static void main(String args[]){
		int a[]={10,8,12,46,78};
		int n=a.length;
		System.out.println(Arrays.toString(a));
		for(int i=1;i<n;i++){
			if(a[i]>a[i-1]){
				int temp=a[i];
				a[i]=a[i-1];
				a[i-1]=temp;
			}
			for(int j=i-1;j-1>=0;j--){
				if(a[j]>a[j-1]){
					int temp=a[j];
					a[j]=a[j-1];
					a[j-1]=temp;
				}
			}
		}
		System.out.println(Arrays.toString(a));
	}
}

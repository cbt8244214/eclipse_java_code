 //user vs user.
import java.util.Scanner;
public class Tic
{
      static char board[][]=new char[3][3];
      static char player;
    static Scanner in=new Scanner(System.in);
    
    public static void main(String args[])
    {
        //Tic Toe=new Tic();
        newBoard();
        play();
    }
    private static void newBoard(){
		 player='X';
			char board1[][] = {{'1', '2', '3'},
				      {'4', '5', '6'},
				      {'7', '8', '9'}};
			for(int i=0;i<3;i++){
				for(int j=0;j<3;j++){
					board[i][j]=board1[i][j];
				}
			}
			currentBoard();
		 }
    public static void currentBoard()
    {
       System.out.println();
    	System.out.println("|---|---|---|"); 
        System.out.println("| " + board[0][0] + " | "+ board[0][1] + " | " + board[0][2]+ " |"); 
        System.out.println("|-----------|"); 
        System.out.println("| " + board[1][0] + " | "+ board[1][1] + " | " + board[1][2]+ " |"); 
        System.out.println("|-----------|"); 
        System.out.println("| " + board[2][0] + " | "+ board[2][1] + " | " + board[2][2]+ " |"); 
        System.out.println("|---|---|---|");
        System.out.println();
     
    }
    
    public static  void play()
    {
        int spot;
        char blank = ' ';
        
        System.out.println(  "Player " + getPlayer() +" will go first and be the letter 'X'" );
        
        do {
           // currentBoard();              // display current board
            
            System.out.println(  "\n Player " + getPlayer() +" turn." );
            
           
            while (true) {
   	         
                spot=in.nextInt();
                
                if(isValidMove(spot)){
                	break;
                }else {
                	System.out.println(spot + " is not a valid move.");
            	}
            }
            placeMove( board,spot, getPlayer());
            currentBoard();                         // display current board
            
            nextPlayer();
        }while ( checkWinner() == blank );
        playagain();
    }
    
    
    public static  char checkWinner()
    {
        char Winner = ' ';
        
        // Check if X wins
        if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') Winner = 'X';
        if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X') Winner = 'X';
        if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X') Winner = 'X';
        if (board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X') Winner = 'X';
        if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X') Winner = 'X';
        if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X') Winner = 'X';
        if (board[0][0] == 'X' &&  board[1][1]  == 'X' && board[2][2] == 'X') Winner = 'X';
        if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') Winner = 'X';
        if (Winner == 'X' )
        {System.out.println("Player-1 wins the game." );
            return Winner;
        }
        
        // Check if O wins
        if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O') Winner = 'O';
        if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O') Winner = 'O';
        if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O') Winner = 'O';
        if (board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O') Winner = 'O';
        if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O') Winner = 'O';
        if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O') Winner = 'O';
        if (board[0][0] == 'O' &&  board[1][1]  == 'O' && board[2][2] == 'O') Winner = 'O';
        if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') Winner = 'O';
        if (Winner == 'O' )
        {
            System.out.println( "Player-2 wins the game." );
        return Winner; }
        
        // check for Tie
       /* for(int i=0;i<3;i++)
        {
        	for(int j=0;j<3;j++){
        		
            if(board[i][j]=='X' || board[i][j]=='O')
            {
                if((i==2)&&(j==2))
                {
                    char Draw='D';
                    System.out.println(" Game is draw ");
                    return Draw;
                } 
                continue;
            }
            else
            break;
            
        }
        
    }*/
        char a='1';
        for(int i=0;i<board.length;i++){
        	for(int j=0;j<board.length;j++){
        		if(board[i][j]==a){
        			return Winner;
        		}
        		a++;
        	}
        }
        char Draw='D';
        System.out.println(" Game is draw ");
        return Draw;
    }
    
	private static boolean isValidMove ( int userInput) {
		switch(userInput) {
		case 1:
		return (board[0][0] == '1');
		case 2:
		return (board[0][1] == '2');
		case 3:
		return (board[0][2] == '3');
		case 4:
		return (board[1][0] == '4');
		case 5:
		return (board[1][1] == '5');
		case 6:
		return (board[1][2] == '6');
		case 7:
		return (board[2][0] == '7');
		case 8:
		return (board[2][1] == '8');
		case 9:
		return (board[2][2] == '9');
		default:
		return false;
		}
		}
    
	static void placeMove(char[][] board, int position, char symbol) {
		switch(position) {
		case 1:
		board[0][0] = symbol;
		break;
		case 2:
		board[0][1] = symbol;
		break;
		case 3:
		board[0][2] = symbol;
		break;
		case 4:
		board[1][0] = symbol;
		break;
		case 5:
		board[1][1] = symbol;
		break;
		case 6:
		board[1][2] = symbol;
		break;
		case 7:
		board[2][0] = symbol;
		break;
		case 8:
		board[2][1] = symbol;
		break;
		case 9:
		board[2][2] = symbol;
		break;
		}
		}
    
    public static  void nextPlayer()
    {
        if (player == 'X')
        player = 'O';
        else player = 'X';
        
    }
    

    
    public static  char getPlayer()
    {
        return player;
    }
    public static void playagain() {
    	String ch;
            System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
            Scanner in =new Scanner(System.in);
            ch=in.nextLine();
            if((ch.equalsIgnoreCase("yes"))){
            	newBoard();
                 play();
            }
            else if (!(ch.equalsIgnoreCase("yes"))) {
                System.out.println("Bye!");
                System.exit(0);  // terminate the program
             }
    }
}
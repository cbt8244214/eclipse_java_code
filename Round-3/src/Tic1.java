import java.util.Scanner;


public class Tic1 {
	static char [][]board=new char[3][3];
	static Scanner in=new Scanner(System.in);
	static char player;
	public static void main(String args[]){
		initializeBoard();
		play();
	}
	public static void initializeBoard(){
		player='X';
		char a='1';
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board.length;j++){
				board[i][j]=a;
				a++;
			}
		}
		System.out.println("Welcome to TicTacToe :\n");
		printBoard();
	}
	public static void printBoard(){
		System.out.println("|-----------|");
		System.out.println("| "+board[0][0]+" | "+board[0][1]+" | "+board[0][2]+" | ");
		System.out.println("|---|---|---|");
		System.out.println("| "+board[1][0]+" | "+board[1][1]+" | "+board[1][2]+" | ");
		System.out.println("|---|---|---|");
		System.out.println("| "+board[2][0]+" | "+board[2][1]+" | "+board[2][2]+" | ");
		System.out.println("|-----------|");
	}
	public static void play(){
		int spot;
		System.out.println(getPlayer()+" will go first "); 
		do{
			while(true){
			System.out.println(getPlayer()+" turn :");
			spot=in.nextInt();
			if(isvalidMove(spot))
				break;
			else
				System.out.println("This place is already taken.Go again :");
			}
			placeMove(spot,getPlayer());
			
			printBoard();
			
			nextPlayer();
		}while(checkWinner()==' ');
		playagain();
	}
	public static void nextPlayer(){
		if(player=='X')
			player='O';
		else
			player='X';
	}
	public static char getPlayer(){
		return player;
	}
	public static boolean isvalidMove(int spot){
		switch(spot){
			case 1:
				return (board[0][0]=='1');
			case 2:
				return (board[0][1]=='2');
			case 3:
				return (board[0][2]=='3');
			case 4:
				return (board[1][0]=='4');
			case 5:
				return (board[1][1]=='5');
			case 6:
				return (board[1][2]=='6');
			case 7:
				return (board[2][0]=='7');
			case 8:
				return (board[2][1]=='8');
			case 9:
				return (board[2][2]=='9');
				default :
					return false;
		}
		
	}
	static void placeMove( int position, char symbol) {
		switch(position) {
		case 1:
		board[0][0] = symbol;
		break;
		case 2:
		board[0][1] = symbol;
		break;
		case 3:
		board[0][2] = symbol;
		break;
		case 4:
		board[1][0] = symbol;
		break;
		case 5:
		board[1][1] = symbol;
		break;
		case 6:
		board[1][2] = symbol;
		break;
		case 7:
		board[2][0] = symbol;
		break;
		case 8:
		board[2][1] = symbol;
		break;
		case 9:
		board[2][2] = symbol;
		break;
		}
		}
    

	 public static  char checkWinner()
	    {
	        char Winner = ' ';
	        
	        
	        if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') Winner = 'X';
	        if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X') Winner = 'X';
	        if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X') Winner = 'X';
	        if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X') Winner = 'X';
	        if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X') Winner = 'X';
	        if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') Winner = 'X';
	        if (Winner == 'X' )
	        {System.out.println("Player-1 wins the game." );
	            return Winner;
	        }
	        
	       
	        if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O') Winner = 'O';
	        if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O') Winner = 'O';
	        if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O') Winner = 'O';
	        if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O') Winner = 'O';
	        if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O') Winner = 'O';
	        if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') Winner = 'O';
	        if (Winner == 'O' )
	        {
	            System.out.println( "Player-2 wins the game." );
	        return Winner; }
	        
	     
	        for(int i=0;i<3;i++)
	        {
	        	for(int j=0;j<3;j++){
	        		
	            if(board[i][j]=='X' || board[i][j]=='O')
	            {
	                if((i==2)&&(j==2))
	                {
	                    char Draw='D';
	                    System.out.println(" Game is draw ");
	                    return Draw;
	                } 
	                continue;
	            }
	            else
	            break;
	            
	        }
	        
	    }
			return Winner;
	    }
	 public static void playagain(){
		 System.out.println("would you like to play again ( enter 'yes') or enter to stop :");
		Scanner in=new Scanner(System.in);
		 String ch=in.nextLine();
		 if((ch.equalsIgnoreCase("yes"))){
			 initializeBoard();
				play();
		 }
		 else if(!(ch.equalsIgnoreCase("yes"))){
			 System.out.println("Bye");
			 System.exit(0);
		 }
	 }
}

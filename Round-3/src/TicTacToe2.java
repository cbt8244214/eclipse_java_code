import java.util.Random;
import java.util.Scanner;
//user vs computer	 
public class TicTacToe2 {
	static char[][] board=new char[3][3];
	static Scanner scanner = new Scanner(System.in);
	public static void main(String[] args) {

	initializeBoard();
	win();
	
	}
	 private static void win(){
		 while (true) {
				playerTurn();
				if (isGameFinished()){
					playagain();
				}
				printBoard();
				computerTurn();
				if (isGameFinished()){
					playagain();
				}
				printBoard();
			}
	 }
	 
private static boolean isGameFinished() {
	if (hasContestantWon( 'X')) {
		printBoard();
		System.out.println("Player wins!");
		return true;
	} 
	if (hasContestantWon( 'O')) {
		printBoard();
		System.out.println("Computer wins!");
		return true;
	}
	char n='1';
	for (int i = 0; i < board.length; i++) {
		for (int j = 0; j < board.length; j++) {
			if (board[i][j] == n) {
				return false;
			}
			n++;
		}
	}
	printBoard();
	System.out.println("The game ended in a tie!");
	return true;
}
	 
	 
	private static boolean hasContestantWon( char symbol) {
	if ((board[0][0] == symbol && board [0][1] == symbol && board [0][2] == symbol) ||
	(board[1][0] == symbol && board [1][1] == symbol && board [1][2] == symbol) ||
	(board[2][0] == symbol && board [2][1] == symbol && board [2][2] == symbol) ||
	(board[0][0] == symbol && board [1][0] == symbol && board [2][0] == symbol) ||
	(board[0][1] == symbol && board [1][1] == symbol && board [2][1] == symbol) ||
	(board[0][2] == symbol && board [1][2] == symbol && board [2][2] == symbol) ||
	(board[0][0] == symbol && board [1][1] == symbol && board [2][2] == symbol) ||
	(board[0][2] == symbol && board [1][1] == symbol && board [2][0] == symbol) ) {
	return true;
	}
	return false;
	}
	 
	 
	private static void computerTurn() {
	Random rand = new Random();
	int computerMove;
	while (true) {
	computerMove = rand.nextInt(9) + 1;
	if (isValidMove( computerMove)) {
	break;
	}
	}
	System.out.println("Computer chose " + computerMove);
	placeMove( computerMove, 'O');
	}
	 
	 
	private static boolean isValidMove ( int userInput) {
	switch(userInput) {
	case 1:
	return (board[0][0] == '1');
	case 2:
	return (board[0][1] == '2');
	case 3:
	return (board[0][2] == '3');
	case 4:
	return (board[1][0] == '4');
	case 5:
	return (board[1][1] == '5');
	case 6:
	return (board[1][2] == '6');
	case 7:
	return (board[2][0] == '7');
	case 8:
	return (board[2][1] == '8');
	case 9:
	return (board[2][2] == '9');
	default:
	return false;
	}
	}
	 
	 static void playerTurn() {
	int userInput;
	while (true) {
	System.out.println("Enter Your Move!!");
	userInput = scanner.nextInt();
	if (isValidMove(userInput)){
	break;
	} else {
	System.out.println(userInput + " is not a valid move.");
	}
	}
	placeMove( userInput, 'X');
	}
	 
	 
	static void placeMove( int position, char symbol) {
	switch(position) {
	case 1:
	board[0][0] = symbol;
	break;
	case 2:
	board[0][1] = symbol;
	break;
	case 3:
	board[0][2] = symbol;
	break;
	case 4:
	board[1][0] = symbol;
	break;
	case 5:
	board[1][1] = symbol;
	break;
	case 6:
	board[1][2] = symbol;
	break;
	case 7:
	board[2][0] = symbol;
	break;
	case 8:
	board[2][1] = symbol;
	break;
	case 9:
	board[2][2] = symbol;
	break;
	}
	}
	 private static void initializeBoard(){
		char a='1';
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				board[i][j]=a;
				a++;
			}
		}
		printBoard();
	 }
	private static void printBoard() {
	System.out.print((board[0][0]));
	System.out.print("|");
	System.out.print((board[0][1]));
	System.out.print("|");
	System.out.println((board[0][2]));
	System.out.println("-----");
	System.out.print((board[1][0]));
	System.out.print("|");
	System.out.print((board[1][1]));
	System.out.print("|");
	System.out.println((board[1][2]));
	System.out.println("-----");
	System.out.print((board[2][0]));
	System.out.print("|");
	System.out.print((board[2][1]));
	System.out.print("|");
	System.out.println((board[2][2]));
	}
	public static void playagain() {
		String ch;
	        System.out.println ("Would you like to play again (Enter 'yes')? or click enter to stop");
	        Scanner in =new Scanner(System.in);
	        ch=in.nextLine();
	        if((ch.equalsIgnoreCase("yes"))){
	        	initializeBoard();
	        	win();
	            
	        }
	        else if (!(ch.equalsIgnoreCase("yes"))) {
	            System.out.println("Bye!");
	            System.exit(0);  // terminate the program
	         }
	}
	}


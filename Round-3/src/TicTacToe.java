import java.util.Random;
import java.util.Scanner;


public class TicTacToe {
	static char [][]board=new char[3][3];
	static Scanner in=new Scanner(System.in);
	public static void main(String args[]){
		initializeBoard();
		win();
	}
	public static void initializeBoard(){
		char a='1';
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board.length;j++){
				board[i][j]=a;
				a++;
			}
		}
		printBoard();
	}
	public static void printBoard(){
		System.out.println("|-----------|");
		System.out.println("| "+board[0][0]+" | "+board[0][1]+" | "+board[0][2]+" | ");
		System.out.println("|---|---|---|");
		System.out.println("| "+board[1][0]+" | "+board[1][1]+" | "+board[1][2]+" | ");
		System.out.println("|---|---|---|");
		System.out.println("| "+board[2][0]+" | "+board[2][1]+" | "+board[2][2]+" | ");
		System.out.println("|-----------|");
	}
	public static void win(){
		while(true){
			playerMove();
			if(isgamefinished()){
				playagain();
			}
			printBoard();
			computerMove();
			if(isgamefinished()){
				playagain();
			}
			printBoard();
		}
	}
	public static void playerMove(){
		int spot;
		while(true){
			System.out.println("\nX turn:\n");
			spot=in.nextInt();
			if(isvalidMove(spot)){
				break;
			}else{
				System.out.println("This position already taken . Go again :");
			}
		}
		placeMove(spot,'X');
	}
	public static void computerMove(){
		Random rand=new Random();
		int comspot;
		while(true){
			
			comspot=rand.nextInt(9)+1;
			if(isvalidMove(comspot)){
				break;
			}
		}
		System.out.println("\nComputer turn: " +comspot+"\n");
		placeMove(comspot,'O');
	}
	public static boolean isvalidMove(int position){
		switch(position){
		case 1:
			return (board[0][0]=='1');
		case 2:
			return (board[0][1]=='2');
		case 3:
			return (board[0][2]=='3');
		case 4:
			return (board[1][0]=='4');
		case 5:
			return (board[1][1]=='5');
		case 6:
			return (board[1][2]=='6');
		case 7:
			return (board[2][0]=='7');
		case 8:
			return (board[2][1]=='8');
		case 9:
			return (board[2][2]=='9');
			default :
				return false;
		}
	}
	public static void placeMove(int position,char symbol){
		switch(position) {
		case 1:
		board[0][0] = symbol;
		break;
		case 2:
		board[0][1] = symbol;
		break;
		case 3:
		board[0][2] = symbol;
		break;
		case 4:
		board[1][0] = symbol;
		break;
		case 5:
		board[1][1] = symbol;
		break;
		case 6:
		board[1][2] = symbol;
		break;
		case 7:
		board[2][0] = symbol;
		break;
		case 8:
		board[2][1] = symbol;
		break;
		case 9:
		board[2][2] = symbol;
		break;
		}
	}
	public static boolean isgamefinished(){
		if(hascontestantwon('X')){
			printBoard();
			System.out.println("Player win");
			return true;
		}
		if(hascontestantwon('O')){
			printBoard();
			System.out.println("computer win");
			return true;
		}
		char a='1';
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board.length;j++){
				if(board[i][j]==a){
					return false;
				}
				a++;
			}
		}
		printBoard();
		System.out.println("Match draw");
		return true;
	}
	public static boolean hascontestantwon(char symbol){
		if ((board[0][0] == symbol && board [0][1] == symbol && board [0][2] == symbol) ||
			(board[1][0] == symbol && board [1][1] == symbol && board [1][2] == symbol) ||
			(board[2][0] == symbol && board [2][1] == symbol && board [2][2] == symbol) ||
			(board[0][0] == symbol && board [1][0] == symbol && board [2][0] == symbol) ||
			(board[0][1] == symbol && board [1][1] == symbol && board [2][1] == symbol) ||
			(board[0][2] == symbol && board [1][2] == symbol && board [2][2] == symbol) ||
			(board[0][0] == symbol && board [1][1] == symbol && board [2][2] == symbol) ||
			(board[0][2] == symbol && board [1][1] == symbol && board [2][0] == symbol) ) {
			return true;
		}
		return false;
	}
	public static void playagain(){
		 System.out.println("would you like to play again ( enter 'yes') or enter to stop :");
			Scanner in=new Scanner(System.in);
			 String ch=in.nextLine();
			 if((ch.equalsIgnoreCase("yes"))){
				 initializeBoard();
					win();
			 }
			 else if(!(ch.equalsIgnoreCase("yes"))){
				 System.out.println("Bye");
				 System.exit(0);
			 }
	}
}

import java.util.StringTokenizer;
public class ReverseOdd2 {
	public static void main(String args[]){
		String s="OBJECT ORIENTED HIGH LEVEL PROGRAMMING LANGUAGE";
		String a[]=s.split(" ");
		for(int i=0;i<a.length;i++){
			if(i%2==1){
				System.out.print(new StringBuffer(a[i]).reverse()+" ");
			}else
				System.out.print(a[i]+" ");
		}
	}
}

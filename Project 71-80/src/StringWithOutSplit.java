//78.Separate String without using Split method
import java.util.StringTokenizer;
public class StringWithOutSplit{
	public static void main(String[] args){
		String s="Red,Green,Blue,Black,White";
		StringTokenizer str=new StringTokenizer(s,",");
		while(str.hasMoreTokens()){
			System.out.println(str.nextToken());
		} 
	}
}
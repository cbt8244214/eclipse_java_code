//76.Reverse the words in odd position.
import java.util.StringTokenizer;
class ReverseOdd{
	public static void main(String args[]){
		String a="object oriented high level programming language";
		String a1[]=a.split(" ");
		for(int i=0;i<a1.length;i++){ 
			if(i%2==0){
				System.out.print(new StringBuffer(a1[i].toUpperCase()).reverse()+" ");
				}
			else{
				System.out.print(a1[i].toUpperCase()+" ");
			}
		}
	}
}
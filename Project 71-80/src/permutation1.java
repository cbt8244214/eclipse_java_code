//77.writre a program to find all permutation of string recursively.
import java.util.Scanner;
public class permutation1 {
	 static void permute(String s,String ans){
		if(s.length()==0){
			System.out.print(ans+" ");
		}
		for(int i=0;i<s.length();i++){
			char ch=s.charAt(i);
			String res=s.substring(0, i)+s.substring(i+1);
			permute(res,ans+ch);
		}
	}
	
	public static void main(String args[]){
		String s;
		String ans="";
		Scanner s1=new Scanner(System.in);
		System.out.print("Enter the string :");
		s=s1.next();
		permute(s,ans);
	}
}
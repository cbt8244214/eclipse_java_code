class A8 {
	void callme1(){
		System.out.println("inside A8's call1 method");
	}
}
class B8 extends A8{
	void callme2(){
		System.out.println("inside B8's callme2 mathod");
	}
}
public class DynamicMethod1{
	public static void main(String args[]){
		A8 a=new A8();
		B8 b=new B8();
		A8 r;
		r=a;
		r.callme1();
		r=b;
		//r.callme2();
		r.callme1();
	}
}

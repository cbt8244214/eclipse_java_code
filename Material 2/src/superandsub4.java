
class A4 {
	void show(){
		System.out.println("Superclass method");
	}
}
class B4 extends A4{
	void show(){
		System.out.println("Subclass method");
	}
}
class Override{
	public static void main(String args[]){
		B4 subOb=new B4();
		subOb.show();
	}
}

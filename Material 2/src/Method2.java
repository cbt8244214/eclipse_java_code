
public class Method2 {
	int y=2;
	int square(){
		return 10*10;
	}
	double square(double i){
		return i*i;
	}
}
class MethodDemo2{
	public static void main(String args[]){
		Method2 ms=new Method2();
			System.out.println("square of 10 is"+ms.square());
			System.out.println("square(5) is"+ms.square(5.0));
			System.out.println("square(y)is"+ms.square(ms.y));
	}
}

interface Callback{
		void callback();
}
	class Client implements Callback {
			public void callback(){
				System.out.println("Interface Method");
			}
			void nonIfaceMeth(){
				System.out.println("Non interface Method");
			}
		}
		class TestIface{
			public static void main(String args[]){
				//Callback c1=new Client();
				Client c=new Client();
				c.callback();
				c.nonIfaceMeth();
			}

	}


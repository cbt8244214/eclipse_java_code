
class A {
	int i;
	int showI(){
		return i;
	}
}
class B extends A{
	int j;
	int showJ(){
		return j;
	}
	int sum(){
		return i+j;
	}
}
class InheritanceDemo{
	public static void main(String args[]){
		A superOb=new A();
		B subOb=new B();
		superOb.i=1;
		System.out.println("Contents of superOb:i="+superOb.showI());
		System.out.println();
		subOb.i=5;
		subOb.j=10;
		System.out.println("Contents of subOb:i="+subOb.showI()+"j="+subOb.showJ());
		System.out.println();
		System.out.println("Sum of i and j in subOb:"+subOb.sum());
	}
}


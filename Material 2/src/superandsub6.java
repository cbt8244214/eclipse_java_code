class A6 {
	void show(){
		System.out.println("Superclass method");
	}
}
class B6 extends A6{
	void show(String t){
		System.out.println(t);
	}
}
class OverloadSuper{
	public static void main(String args[]){
		B6 subOb=new B6();
		subOb.show("Subclass method");
		subOb.show();
	}
}


public class Test3 {
    int a;
    Test3(int i){
    	a=i;
    }
    Test3 incrByTen(){
    	Test3 b =new Test3(a+10);
    	return b;
    }
}
class RetOb{
	public static void main(String args[]){
		Test3 ob1=new Test3(5);
		Test3 ob2;
		ob2=ob1.incrByTen();
		System.out.println("ob1.a:"+ob1.a);
		System.out.println("ob2.a:"+ob2.a);
	}
}

class A5 {
	void show(){
		System.out.println("Superclass method");
	}
}
class B5 extends A5{
	void show(){
		super.show();
		System.out.println("Subclass method");
	}
}
class Override1{
	public static void main(String args[]){
		B5 subOb=new B5();
		subOb.show();
	}
}

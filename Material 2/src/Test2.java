
public class Test2 {
	int a;
	Test2(int i){
		a=i;
	}
	void meth(Test2 o){
		o.a+=5;
	}
}
class CallRef{
	public static void main(String aargs[]){
		Test2 ob=new Test2(5);
		System.out.println("ob.a before call method:"+ob.a);
		ob.meth(ob);
		System.out.println("ob.a after call method:"+ob.a);
	}
	
}

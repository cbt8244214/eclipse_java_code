abstract class A9 {
	abstract void callme();
	void callmetoo(){
		System.out.println("This is a concrete method");
	}
}
class B9 extends A9{
	void callme(){
		System.out.println("B9's implementation of callme");
	}
}
 public class Abstract1{
	public static void main(String args[]){
		B9 b=new B9();
		b.callme();
		b.callmetoo();
	}
}

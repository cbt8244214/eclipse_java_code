interface A11 {
	void meth1();
}
interface B11 {
	void meth2();
}
interface C11 extends A11 ,B11{
	void meth3();
}
class Myclass implements C11{
	public void meth1(){
		System.out.println("Implements meth1()");
	}
	public void meth2(){
		System.out.println("Implement meth2()");
	}
	public void meth3(){
		System.out.println("Implement meth3()");
	}
}
public class Interface2{
	public static void main(String args[]){
		Myclass ob=new Myclass();
		ob.meth1();
		ob.meth2();
		ob.meth3();
	}
}
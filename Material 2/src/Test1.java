
public class Test1 {
	void meth(int i){
		i+=5;
	}
}
class CallbyValue{
	public static void main(String args[]){
		Test1 ob=new Test1();
		int a=5;
		System.out.println("a before call method:"+a);
		ob.meth(a);
		System.out.println("a after call method:"+a);
	}
}

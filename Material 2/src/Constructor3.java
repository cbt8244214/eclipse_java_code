
public class Constructor3 {
	int a;
	Constructor3(){
		a=0;
	}
	Constructor3(int i){
		a=i;
	}
	int meth(){
		return a;
	}
}
class ConstructorDemo3{
	public static void main(String args[]){
		Constructor3 c=new Constructor3();
		Constructor2 c1=new Constructor2(5);
		System.out.println("c.meth()is "+c.meth());
		System.out.println("c.meth()is "+c1.meth());
	}
}

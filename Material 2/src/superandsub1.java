
class A1 {
	int i;
	A1(int a){
		i=a;
	}
	void showI(){
		System.out.println("i:"+i);
	}
}
class B1 extends A1{
	int j;
	B1(int a,int b){
		super(a);
		j=b;
	}
	void showJ(){
		System.out.println("j:"+j);
	}
}
class Super{
	public static void main(String args[]){
		B1 subOb=new B1(5,10);
		subOb.showI();
		subOb.showJ();
	}
}

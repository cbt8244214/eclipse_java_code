
public class Overload {
	void test(){
		System.out.println("No parameters");
	}
	void test(int a,int b){
		System.out.println("a and b:"+a+" "+b);
	}
	double test(double a){
		return a*a;
	}
}
class OverloadDemo{
	public static void main(String args[]){
		Overload ob=new Overload();
		ob.test();
		ob.test(10,20);
		System.out.println("Result of ob.test(2.0):"+ob.test(2.0));
		System.out.println("Result of ob.test(5):"+ob.test(5));
	}
}

 class A3{
	A3(){
		System.out.println("inside A3's constructor.");
	}
}
 class B3 extends A3{
	 int j;
	 B3(){
		 System.out.println("inside B3's constructor.");
	 }
 }
 class C3 extends B3{
	 C3(){
		 System.out.println("inside C3's constructor");
	 }
 }
 class Super2{
	 public static void main(String args[]){
		 C3 c=new C3();
	 }
 }

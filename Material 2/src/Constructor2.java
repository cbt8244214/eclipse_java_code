
public class Constructor2 {
	int a;
	Constructor2(){
		a=0;
	}
	Constructor2 (int i){
		a=i;
	}
	int meth(){
		return a;
	}
}
class ConstructorDemo2{
	public static void main(String args[]){
		Constructor2 c1=new Constructor2(5);
		Constructor2 c=new Constructor2();
		System.out.println("c1.meth()="+c1.meth());
		System.out.print("c.meth()="+c.meth());
	}
}

//136.first repeated and non repeated character in a given string.
import java.util.*;
public class Repeatedcharacter {
	public static void main(String args[]){
		String str="Javaconceptoftheday";
		LinkedHashMap <Character,Integer>hm=new LinkedHashMap<Character,Integer>();
		char ch[]=str.toCharArray();
		for(char c:ch){
			if(hm.containsKey(c)){
				hm.put(c, hm.get(c)+1);
			}else
				hm.put(c, 1);
		}/*for(char c:ch){
			if(hm.get(c)==1)
				System.out.println("Non Repeated character :"+c);
			}
			for(char c:ch){
				if(hm.get(c)>1)
					System.out.println("Repeated character :"+c);
		
		}*/
		Set<Map.Entry<Character,Integer>>s=hm.entrySet();
		for(Map.Entry<Character,Integer>s1:s){
			if(s1.getValue()>1){
				System.out.println("Repeated character :"+s1);
				break;
			}else if(s1.getValue()==1){
				System.out.println("Non Repeated character :"+s1);
				//break;
			}
		}
	}
}

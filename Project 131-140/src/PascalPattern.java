 //135.write a program to print pascal triangle.
public class PascalPattern{
	public static int fact(int num){
		int f=1,i=1;
		while(i<=num){
			f=f*i;
			i++;
		}
		return f;	
	}
	public static void main(String[] args){
		int line=4;
		for(int i=0;i<line;i++){
			for(int j=0;j<=i;j++){
				System.out.print(fact(i)/(fact(j)*fact( i-j)));
			}
			System.out.println();
		}
	}
}
//132.write a program to find the highest sequence of repeated elements.
import java.util.*;
public class ArrangeNumber {
	public static void main(String args[]){
		int k;
		int a[]={8,7,4,1,0,-2,-1,-4,-6};
		System.out.println(Arrays.toString(a));
		for(int i=0;i<a.length;i++){
			k=0;
			for(int j=i+1;j<a.length;j++){
				if(a[i]=='*'){
					k++;
					break;
				}
				if(Math.abs(a[i])==Math.abs(a[j])){
					System.out.print(a[i]+" "+a[j]+" ");
					a[j]='*';
					k++;
				}
			}
			if(k==0)
				System.out.print(a[i]+" ");
		}
	}
}

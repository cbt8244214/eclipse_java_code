//133.write a program to find the highest sequence of repeated elements.
public class HighestSequence{
	public static void main(String args[]){
		char array[]={'a','5','c','s','e','9','A','b','a','5','c','s','e'};
		for(int i=0;i<array.length;i++){
			for(int j=i+1;j<array.length;j++){
				if(array[i]==array[j]){
					System.out.print(i);
					System.out.println(" "+j);
				}
			}
		}
	}
}
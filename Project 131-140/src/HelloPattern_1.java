//138. print the hello pattern.
public class HelloPattern_1 {
	public static void main(String args[]){
		String s1="HELLO";
		String s= new StringBuffer(s1).deleteCharAt(0).reverse()+s1;
		char c[]=s.toCharArray();
		int k=s.length(); 
		int k1=s.length()-1;
		for(int i=0;i<k;i++){
			for(int j=0;j<k;j++){
				if(i==j)
				System.out.print(c[i]);	
				else if(k1==j)
					System.out.print(c[k1]);
				else
					System.out.print(" ");	
			}
			k1--;
			System.out.println();	
		}
	}
}

//138. print the hello pattern.
public class HelloPattern_{
	public static void main(String args[]){
		String s1="Hello";
		String s=new StringBuffer(s1).deleteCharAt(0).reverse()+s1;
		int k=s.length();	//k=9;							//olle  =>hello=>ollehello
		int k1=s.length()-1;  //k1=8;
		char c[]=s.toCharArray();			// c ={o,l,l,e,h,e,l,l,o}
		for(int i=0;i<k;i++){
			for(int j=0;j<k;j++){
				if(i==j)
					System.out.print(c[i]);
				else if(k1==j)
					System.out.print(c[k1]);
				else 
					System.out.print(" ");
			}
			k1--;
			System.out.println(" ");
		}
	}
}
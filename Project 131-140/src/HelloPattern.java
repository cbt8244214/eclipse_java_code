//138.print the hello pattern.
import java.util.Scanner;
public class HelloPattern{
	public static void main(String[] args){
		System.out.println("Enter the String");
		Scanner sc=new Scanner(System.in);
		String s1=sc.next();
		//int n=10;
		int n=9;
		//String s=s1+new StringBuffer(s1).reverse();
		String s= new StringBuffer(s1).deleteCharAt(0).reverse()+s1;
		char c[]=s.toCharArray();
		char c1[][]={c,c,c,c,c,c,c,c,c};
		for(int i=0;i<n;i++){
			for(int j=0;j<n; j++){
				if(i==j)
					System.out.print(c1[i][j]);
				else if((i+j)==(n-1))
					System.out.print(c1[i][j]);
				else
					System.out.print(" ");
			}
			System.out.println(" ");
		}
	}
}

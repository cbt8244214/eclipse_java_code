//95.write a program to find ncr and npr.
import java.util.Scanner;
public class NCR_NPR{
	public static double fact(double num){
		double fact=1; 
		for(int i=1;i<=num;i++){
			 fact=fact*i;
		} 
		return fact;
	}
	public static void main(String[] args){
		//int n,r;
 		Scanner scanner=new Scanner(System.in);
		System.out.print("Enter value of n :");
		int n=scanner.nextInt();
		System.out.print("Enter value of r :");
		int r=scanner.nextInt();
		System.out.println("NCR is "+(fact(n)/(fact(n-r)*fact(r))));
		System.out.println("PNR is "+(fact(n/(fact(n-r)))));
	}
}
//91.Consider two arrays. bob=[1,2,3] , alice=[4,2,6].
class TwoArrays{
	public static void main(String args[]){
		int bob[]={1,2,3};
		int alice[]={4,2,6};
		int bobScore=0;
		int aliceScore=0;
		for(int i=0;i<3;i++){
			if(bob[i]<alice[i]){
				aliceScore++;
			}
			else if(bob[i]>alice[i]){
				bobScore++;
			}
		}
		System.out.println(bobScore);
		System.out.println(aliceScore);
	} 
}
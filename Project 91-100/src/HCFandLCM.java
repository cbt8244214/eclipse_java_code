//94.write a program to find HCF and LCM
import java.util.Scanner;
public class HCFandLCM{
	public static void main(String args[]){
		int a,b,lcm,hcf=0;
		Scanner s=new Scanner(System.in);
		System.out.print("enter first number:");
		a=s.nextInt();
		System.out.print("enter second number:");
		b=s.nextInt();
		for(int i=1;i<=a&&i<=b;i++){
			if(a%i==0&&b%i==0){
				hcf=i;
			}
		}
		System.out.println("Hcf of numbers:"+hcf);
		lcm=(a*b)/hcf;
		System.out.print("Lcm of numbers:"+lcm);
	}
}
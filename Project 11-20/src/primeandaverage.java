//15.Find 1 to 100 prime numbers and calculate the average.
class primeandaverage {
	public static void main (String args[]){
		int sum=0,a=0;float avg;
		for(int i=2;i<=100;i++){			//2<=100   //3<=100	 //4<=100
			boolean b=true;					
				for(int j=2;j<i;j++){		//2<2		//2<3	//2<4
					if(i%j==0){							//3%2==0 (j iterate utill inner for loop 
						b=false;								//condition became false).
						break;									//4%2==0  => b=false;
					}											//if(false==true)
				}										 //condition false.so,it goes to the outer loop.
				if(b==true){										
					System.out.print(i+" ");//i=2		//i=3
					sum=sum+i;				//sum=0+2=2 //sum=2+3=5
					a++;					//1			//2
				}
		}
		System.out.println("\n"+sum);
		avg=sum/a;
		System.out.println(avg);
	}
}

//44.Find the sum of given series.
//1/1!+1/2!+1/3!+1/4!+....+n
public class Factorial{
	public static void main(String args[]){
		int val=4;
		double sum=0,f=1;
			for(int i=1;i<=val;i++){
				f=f*i;					//f=1*1; =>1=>2=>6=>24
				sum=sum+(1/f);	
				System.out.print(sum+"+ ");//sum=0+(1/1); =>1=>1.5=>1.65=>
			}
			
	}
}
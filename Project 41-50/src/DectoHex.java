//45.Decimal to Hexadecimal using stack.
import java.util.Scanner;
import java.util.Stack;
public class DectoHex{
	public static void main(String[] args){
		System.out.print("Enter the Decimal number :");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
		int rem=0;
		Stack<String>stk=new Stack<String>();
			String str=" ";
		char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			while(num>0){
				rem=num%16;
				str=hex[rem]+str;
				num=num/16;
			}
				System.out.println("pushed element:");
				System.out.println(stk.push(str));
			while(!(stk.isEmpty())){
				System.out.println("poped element:");
				System.out.println(stk.pop());
			}
	}
}	
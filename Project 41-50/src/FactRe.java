//48.FIND THE FACTORIAL USING RECURSION.
import java.util.Scanner;
public class FactRe {
	static int getch(int n){
		if(n==0){
			return 1;
		}else{
			return n*getch(n-1);
		}
	}
	public static void main(String args[]){
		int n;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the factorial number :");
		n=s.nextInt();
		System.out.println(getch(n));
	}
}

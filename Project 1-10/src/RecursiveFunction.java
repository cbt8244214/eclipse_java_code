//10.SUM OF DIGITS USING RECURSIVE FUNCTION
public class RecursiveFunction{
	private static int sumofDigits(int num){
		if(num==0){
			return 0;
		}
		return num%10+sumofDigits(num/10);
	}
	public static void main(String args[]){
		int sum=sumofDigits(2167);
		System.out.println("SumOfDigitsis="+sum);
	}
}
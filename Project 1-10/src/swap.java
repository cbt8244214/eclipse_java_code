 //6.Swap the two numbers without using temporary variable.
 class swap{
	public static void main(String args[]){
		int a=5;
		int b=8;
		System.out.println("Before swap the numbers:a="+a+" ,b="+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swap the numbers:a="+a+" ,b="+b);
	}
}
//5.check even or odd without using modulus nd division
class OddorEven2{
	public static void main(String args[]){
		int n=23;						//using binary value
		if((n&1)==1){    /*the binary value of 2=10,3=11,4=100,5=101...,10=1010,11=1011
						 the binary value of even numbers are end in 0 value.
						 the binary value of odd numbers are end in 1 value.*/			 
			System.out.println(n+" is odd number");
		}else
			System.out.println(n+" is even number");
	}
} 
//1.write  program to find the factorial of given number
import java.util.Scanner;
class factorial{  
	public static void main(String args[]){
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number :");
		int n=s.nextInt();//n=5;=>5*4*3*2*1=120
		int fact=1;
			for(int i=1;i<=n;i++){
				fact=fact*i;/* 1*1=1
							   1*2=2
							   2*3=6
							   6*4=24
							   24*5=120 */						
			}
			System.out.println("fact="+fact);
		}
	}
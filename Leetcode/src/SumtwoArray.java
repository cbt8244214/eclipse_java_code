import java.util.*;
public class SumtwoArray {
    static int[] twoSum(int num[], int target) {
    	int t[]=new int[2];
    	int k=0;
      for(int i=0;i<num.length;i++){
    	  for(int j=i+1;j<num.length;j++){
    		  if(num[i]+num[j]==target){
    			  t[k]=i;
    			  k++;
    			  t[k]=j;
    		  }
    	  }
      }
      return t;
    }
    public static void main(String args[]){
        int num[]={2,7,11,15};
        int target=9;
        int b[]=twoSum(num,target);
        System.out.print(Arrays.toString(b));
    }
}
import java.util.*;
public class LongestSubstring {
    public static int lengthOfLongestSubstring(String s) {
        int n = s.length();
        int res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (checkRepetition(s, i, j)) {
                    res = Math.max(res, j - i + 1);
                }
            }
        }
        return res;
    }
    private static boolean checkRepetition(String s, int start, int end) {
        Set<Character> chars = new HashSet<Character>();

        for (int i = start; i <= end; i++) {
            char c = s.charAt(i);
            if(chars.contains(c)){
                return false;
            }
            chars.add(c);
        }
        return true;
    }
    public static void main(String args[]){
    	String s="abcabcbb";
    	int a=lengthOfLongestSubstring(s);
    	System.out.print(a);
    }
}
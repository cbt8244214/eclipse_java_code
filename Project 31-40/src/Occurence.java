/*36.Find the number of occurences of the array element.
input= int a[]={1,2,3,1,1,3}
o/p = 1-3 times,2-1 time,3-2 times. */
public class Occurence {
	public static void main(String[]args){
		int a[]={1,2,3,1,1,3};
		int b[]=new int[a.length];
	int v=-1;
		for(int i=0;i<a.length;i++){
			int count=1;
			for(int j=i+1;j<a.length;j++){
				if(a[i]==a[j]){
					count++;
					b[j]=v;
				}
			}
			if(b[i]!=v)
				b[i]=count;
		}
		for(int i=0;i<b.length;i++){
			if(b[i]!=v)
				System.out.println(a[i]+"-"+b[i]+"times");
		}
	}
}
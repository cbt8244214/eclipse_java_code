//38.PROGRAM FOR ANAGRAM
import java.util.Arrays;
public class Anagram{
	public static void main(String[] args){
		String s1="listen";
		String s2="silent";
		boolean c=isAnagram(s1,s2);
		if(c){
			System.out.println(s1+" and "+s2+" are anagram");
		}else
			System.out.println(s1+" and "+s2+" are not anagram");
	}
public static boolean isAnagram(String s1,String s2){
	char c1[]=s1.toCharArray();
	char c2[]=s2.toCharArray();
	Arrays.sort(c1);
	Arrays.sort(c2);
		if(Arrays.toString(c1).equals(Arrays.toString(c2)))
			return true;
			return false;
	}
}



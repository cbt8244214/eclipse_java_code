
import java.util.*;
public class Occurence1 {
	public static void main(String args[]){
		//int a[]={1,2,3,1,1,3};
		int a[]={10,20,20,30,30,40,50,50};
		LinkedHashMap <Integer,Integer> lhs=new LinkedHashMap<Integer,Integer>();
		for(int l:a){
			if(!(lhs.containsKey(l))){
				lhs.put(l, 1);
			}else
				lhs.put(l, lhs.get(l)+1);
		}
		System.out.println(lhs);
		Set<Map.Entry<Integer,Integer>>l=lhs.entrySet();
		for(Map.Entry<Integer, Integer>m:l){
			if(m.getValue()>=1){
				//System.out.println(m.getKey()+"-"+m.getValue()+" times");
				System.out.print(m.getKey()+" ");
			}
		}
	}
}

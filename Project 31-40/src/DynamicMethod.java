//31.Write a program to explain the concept of dynamic dispatch method or Run time polymorphism.
class A3{
	void callme(){
		System.out.println("Inside A3's callme method");
	}
}
class B3 extends A3{
	void callme(){
		System.out.println("Inside B3's callme method");
	}
}
public class DynamicMethod {
	public static void main (String args[]){
		A3 a=new A3();
		B3 b=new B3();
		A3 r;
		r=a;
		r.callme();
		r=b;
		r.callme();
	}
}
